var lut;

THREE.EdgesGeometry = function ( object, hex, thresholdAngle ) {
	var color = ( hex !== undefined ) ? hex : 0xffffff;
	thresholdAngle = ( thresholdAngle !== undefined ) ? thresholdAngle : 1;

	var thresholdDot = Math.cos( THREE.Math.degToRad( thresholdAngle ) );

	var edge = [ 0, 0 ], hash = {};
	var sortFunction = function ( a, b ) { return a - b };

	var keys = [ 'a', 'b', 'c' ];
	var geometry = new THREE.BufferGeometry();

	var vertices = object.geometry.vertices;
	var faces = object.geometry.faces;
	var numEdges = 0;

	for ( var i = 0, l = faces.length; i < l; i ++ ) {

		var face = faces[ i ];

		for ( var j = 0; j < 3; j ++ ) {

			edge[ 0 ] = face[ keys[ j ] ];
			edge[ 1 ] = face[ keys[ ( j + 1 ) % 3 ] ];
			edge.sort( sortFunction );

			var key = edge.toString();

			if ( hash[ key ] === undefined ) {

				hash[ key ] = { vert1: edge[ 0 ], vert2: edge[ 1 ], face1: i, face2: undefined };
				numEdges ++;

			} else {

				hash[ key ].face2 = i;

			}

		}

	}

	var coords = new Float32Array( numEdges * 2 * 3 );

	var index = 0;

	for ( var key in hash ) {

		var h = hash[ key ];

		if ( h.face2 === undefined || faces[ h.face1 ].normal.dot( faces[ h.face2 ].normal ) <= thresholdDot )
		//if ( h.face2 === undefined)
		{
			var vertex = vertices[ h.vert1 ];
			coords[ index ++ ] = vertex.x;
			coords[ index ++ ] = vertex.y;
			coords[ index ++ ] = vertex.z;

			vertex = vertices[ h.vert2 ];
			coords[ index ++ ] = vertex.x;
			coords[ index ++ ] = vertex.y;
			coords[ index ++ ] = vertex.z;

		}

	}

	geometry.addAttribute( 'position', new THREE.BufferAttribute( coords, 3 ) );

	THREE.Line.call( this, geometry, new THREE.LineBasicMaterial( { color: color } ), THREE.LinePieces );

	this.matrix = object.matrixWorld;
	this.matrixAutoUpdate = false;
};

THREE.EdgesGeometry.prototype = Object.create( THREE.Line.prototype );
THREE.EdgesGeometry.prototype.constructor = THREE.EdgesGeometry;

THREE.ChildEdgesGeometry = function ( object, hex, thresholdAngle ) {

	var color = ( hex !== undefined ) ? hex : 0xffffff;
	thresholdAngle = ( thresholdAngle !== undefined ) ? thresholdAngle : 1;

	var thresholdDot = Math.cos( THREE.Math.degToRad( thresholdAngle ) );

	var edge = [ 0, 0 ], hashes = [];
	var sortFunction = function ( a, b ) { return a - b };

	var keys = [ 'a', 'b', 'c' ];
	var geometry = new THREE.BufferGeometry();

	var subChilds = object.children;
	var numEdges = 0;
	for(var k = 0 ; k < subChilds.length ; k++)
	{
		var faces = subChilds[k].geometry.faces;
		var hash = {};
		for ( var i = 0, l = faces.length; i < l; i ++ )
		{
			var face = faces[ i ];

			for ( var j = 0; j < 3; j ++ )
			{
				edge[ 0 ] = face[ keys[ j ] ];
				edge[ 1 ] = face[ keys[ ( j + 1 ) % 3 ] ];
				edge.sort( sortFunction );

				var key = edge.toString();

				if ( hash[ key ] === undefined )
				{
					hash[ key ] = { vert1: edge[ 0 ], vert2: edge[ 1 ], face1: i, face2: undefined };
					numEdges ++;
				}
				else
				{
					hash[ key ].face2 = i;
				}
			}
		}
		hashes.push(hash);
	}

	var coords = new Float32Array( numEdges * 2 * 3 );
	var index = 0;
	for(var k = 0 ; k < subChilds.length ; k++)
	{
		var vertices = subChilds[k].geometry.vertices;
		hash = hashes[k];

		for ( var key in hash )
		{
			var h = hash[ key ];

			if ( h.face2 === undefined)
			{
				var vertex = vertices[ h.vert1 ];
				coords[ index ++ ] = vertex.x;
				coords[ index ++ ] = vertex.y;
				coords[ index ++ ] = vertex.z;

				vertex = vertices[ h.vert2 ];
				coords[ index ++ ] = vertex.x;
				coords[ index ++ ] = vertex.y;
				coords[ index ++ ] = vertex.z;
			}
		}
	}

	geometry.addAttribute( 'position', new THREE.BufferAttribute( coords, 3 ) );

	THREE.Line.call( this, geometry, new THREE.LineBasicMaterial( { color: color } ), THREE.LinePieces );

	this.matrix = object.matrixWorld;
	this.matrixAutoUpdate = false;

};

THREE.ChildEdgesGeometry.prototype = Object.create( THREE.Line.prototype );
THREE.ChildEdgesGeometry.prototype.constructor = THREE.ChildEdgesGeometry;

THREE.SGridWireGeometry = function (widthSegments, heightSegments, depthSegments, node, wireColor) {
	var xSeg = widthSegments+1;
	var ySeg = heightSegments+1;
	var zSeg = depthSegments+1;

	var geometry = new THREE.BufferGeometry();
	var coords = new Float32Array( (xSeg*ySeg + zSeg*xSeg + zSeg*ySeg) * 2 * 3 );

	var index = 0;

	var nearIndex = 0;
	var farIndex = xSeg*ySeg*(zSeg-1);
	for(var y = 0 ; y < ySeg ; y++)
	{
		for(var x = 0 ; x < xSeg ; x++)
		{
			coords[ index ++ ] = node[nearIndex][0];
			coords[ index ++ ] = node[nearIndex][1];
			coords[ index ++ ] = node[nearIndex][2];

			coords[ index ++ ] = node[farIndex][0];
			coords[ index ++ ] = node[farIndex][1];
			coords[ index ++ ] = node[farIndex][2];

			nearIndex ++;
			farIndex ++;
		}

	}

	for(var z = 0 ; z < zSeg ; z++)
	{
		nearIndex = z*(xSeg*ySeg);
		farIndex = z*(xSeg*ySeg) + xSeg*(ySeg-1);
		for(var x = 0 ; x < xSeg ; x++)
		{
			coords[ index ++ ] = node[nearIndex][0];
			coords[ index ++ ] = node[nearIndex][1];
			coords[ index ++ ] = node[nearIndex][2];

			coords[ index ++ ] = node[farIndex][0];
			coords[ index ++ ] = node[farIndex][1];
			coords[ index ++ ] = node[farIndex][2];

			nearIndex ++;
			farIndex ++;
		}
	}

	for(var z = 0 ; z < zSeg ; z++)
	{
		for(var y = 0 ; y < ySeg ; y++)
		{
			nearIndex = z*(xSeg*ySeg)+(y*xSeg);
			farIndex = z*(xSeg*ySeg) +(y*xSeg) + xSeg-1;

			coords[ index ++ ] = node[nearIndex][0];
			coords[ index ++ ] = node[nearIndex][1];
			coords[ index ++ ] = node[nearIndex][2];

			coords[ index ++ ] = node[farIndex][0];
			coords[ index ++ ] = node[farIndex][1];
			coords[ index ++ ] = node[farIndex][2];
		}
	}

	geometry.addAttribute( 'position', new THREE.BufferAttribute( coords, 3 ) );

	THREE.Line.call( this, geometry, new THREE.LineBasicMaterial( { color: wireColor } ), THREE.LinePieces );
};

THREE.SGridWireGeometry.prototype = Object.create( THREE.Line.prototype );
THREE.SGridWireGeometry.prototype.constructor = THREE.SGridWireGeometry;

THREE.VoxetWireGeometry = function (xUnit, yUnit, zUnit, nx, ny, nz, wireColor){
	var xSeg = nx+1;
	var ySeg = ny+1;
	var zSeg = nz+1;

	var geometry = new THREE.BufferGeometry();
	var coords = new Float32Array( (xSeg*ySeg + zSeg*xSeg + zSeg*ySeg) * 2 * 3 );

	var index = 0;

	var nearPos = 0;
	var farPos = zUnit * nz;
	for(var y = 0 ; y < ySeg ; y++)
	{
		for(var x = 0 ; x < xSeg ; x++)
		{
			coords[ index ++ ] = xUnit*x;
			coords[ index ++ ] = yUnit*y;
			coords[ index ++ ] = nearPos;

			coords[ index ++ ] = xUnit*x;
			coords[ index ++ ] = yUnit*y;
			coords[ index ++ ] = farPos;
		}
	}

	nearPos = 0;
	farPos = yUnit * ny;
	for(var z = 0 ; z < zSeg ; z++)
	{
		for(var x = 0 ; x < xSeg ; x++)
		{
			coords[ index ++ ] = xUnit*x;
			coords[ index ++ ] = 0;
			coords[ index ++ ] = zUnit*z;

			coords[ index ++ ] = xUnit*x;
			coords[ index ++ ] = farPos;
			coords[ index ++ ] = zUnit*z;
		}
	}

	nearPos = 0;
	farPos = xUnit * nx;
	for(var z = 0 ; z < zSeg ; z++)
	{
		for(var y = 0 ; y < ySeg ; y++)
		{
			coords[ index ++ ] = 0;
			coords[ index ++ ] = yUnit*y;
			coords[ index ++ ] = zUnit*z;

			coords[ index ++ ] = farPos;
			coords[ index ++ ] = yUnit*y;
			coords[ index ++ ] = zUnit*z;
		}
	}

	geometry.addAttribute( 'position', new THREE.BufferAttribute( coords, 3 ) );

	THREE.Line.call( this, geometry, new THREE.LineBasicMaterial( { color: wireColor } ), THREE.LinePieces );
};

THREE.VoxetWireGeometry.prototype = Object.create( THREE.Line.prototype );
THREE.VoxetWireGeometry.prototype.constructor = THREE.VoxetWireGeometry;

THREE.VoxetGeometry = function ( width, height, depth, xdir, ydir, zdir, widthSegments, heightSegments, depthSegments ) {

	THREE.Geometry.call( this );

	this.type = 'VoxetGeometry';

	this.parameters = {
		width: width,
		height: height,
		depth: depth,
		widthSegments: widthSegments,
		heightSegments: heightSegments,
		depthSegments: depthSegments
	};

	this.widthSegments = widthSegments || 1;
	this.heightSegments = heightSegments || 1;
	this.depthSegments = depthSegments || 1;

	var scope = this;

	var width_half = width / 2;
	var height_half = height / 2;
	var depth_half = depth / 2;
	
	var xyz = xdir*ydir*zdir;
	buildPlane( 'z', 'y', - 1, - 1, zdir*depth, ydir*height, xdir*width, xyz, 0 ); // px
	buildPlane( 'z', 'y',   1, - 1, zdir*depth, ydir*height, 0, xyz, 1 ); // nx
	buildPlane( 'x', 'z',   1,   1, xdir*width, zdir*depth, ydir*height, xyz, 2 ); // py
	buildPlane( 'x', 'z',   1, - 1, xdir*width, zdir*depth, 0, xyz, 3 ); // ny
	buildPlane( 'x', 'y',   1, - 1, xdir*width, ydir*height, zdir*depth, xyz, 4 ); // pz
	buildPlane( 'x', 'y', - 1, - 1, xdir*width, ydir*height, 0, xyz, 5 ); //nz

	function buildPlane( u, v, udir, vdir, width, height, depth, direction, materialIndex ) {

		var w, ix, iy,
		gridX = scope.widthSegments,
		gridY = scope.heightSegments,
		width_half = width,
		height_half = height,
		offset = scope.vertices.length;

		if ( ( u === 'x' && v === 'y' ) || ( u === 'y' && v === 'x' ) ) {

			w = 'z';

		} else if ( ( u === 'x' && v === 'z' ) || ( u === 'z' && v === 'x' ) ) {

			w = 'y';
			gridY = scope.depthSegments;

		} else if ( ( u === 'z' && v === 'y' ) || ( u === 'y' && v === 'z' ) ) {

			w = 'x';
			gridX = scope.depthSegments;

		}

		var gridX1 = gridX + 1,
		gridY1 = gridY + 1,
		segment_width = width / gridX,
		segment_height = height / gridY,
		normal = new THREE.Vector3();

		normal[ w ] = ((materialIndex & 1) == 0) ? 1 : - 1;
		
		for ( iy = 0; iy < gridY1; iy ++ ) {

			for ( ix = 0; ix < gridX1; ix ++ ) {

				var vector = new THREE.Vector3();
				if(udir < 0 )
					vector[ u ] = ( ix * segment_width - width_half ) * udir;
				else
					vector[ u ] = ( ix * segment_width) * udir;
				if(vdir < 0)
					vector[ v ] = ( iy * segment_height - height_half ) * vdir;
				else
					vector[ v ] = ( iy * segment_height ) * vdir;
				vector[ w ] = depth;

				scope.vertices.push( vector );

			}

		}

		for ( iy = 0; iy < gridY; iy ++ ) {

			for ( ix = 0; ix < gridX; ix ++ ) {

				var a = ix + gridX1 * iy;
				var b = ix + gridX1 * ( iy + 1 );
				var c = ( ix + 1 ) + gridX1 * ( iy + 1 );
				var d = ( ix + 1 ) + gridX1 * iy;

				var uva = new THREE.Vector2( ix / gridX, 1 - iy / gridY );
				var uvb = new THREE.Vector2( ix / gridX, 1 - ( iy + 1 ) / gridY );
				var uvc = new THREE.Vector2( ( ix + 1 ) / gridX, 1 - ( iy + 1 ) / gridY );
				var uvd = new THREE.Vector2( ( ix + 1 ) / gridX, 1 - iy / gridY );

				var face;
				if(direction >= 0)
					face = new THREE.Face3( a + offset, b + offset, d + offset );
				else
					face = new THREE.Face3( d + offset, b + offset, a + offset );
				face.materialIndex = materialIndex;

				scope.faces.push( face );
				
				if(direction >= 0)
					face = new THREE.Face3( b + offset, c + offset, d + offset );
				else
					face = new THREE.Face3( d + offset, c + offset, b + offset );
				face.materialIndex = materialIndex;

				scope.faces.push( face );
			}

		}

	}

	this.mergeVertices();

	scope.verticesNeedUpdate = true;
	scope.normalsNeedUpdate = true;
	scope.computeBoundingSphere();
	scope.computeFaceNormals();
};

THREE.VoxetGeometry.prototype = Object.create( THREE.Geometry.prototype );
THREE.VoxetGeometry.prototype.constructor = THREE.VoxetGeometry;


THREE.SGridGeometry = function (widthSegments, heightSegments, depthSegments, node) {
	THREE.Geometry.call( this );
	
	this.type = 'SGridGeometry';
	
	this.parameters = {
		widthSegments: widthSegments,
		heightSegments: heightSegments,
		depthSegments: depthSegments
	}
	
	this.widthSegments = widthSegments || 1;
	this.heightSegments = heightSegments || 1;
	this.depthSegments = depthSegments || 1;
	
	var xdir = node[0][0] < node[widthSegments][0] ? 1 : -1;
	var ydir = node[0][1] < node[widthSegments+1][1] ? 1 : -1;
	var zdir = node[0][2] < node[(widthSegments+1)*(heightSegments+1)][2] ? 1: -1;
	var xyz = xdir*ydir*zdir;
	
	var scope = this;
	
	buildPlane( 'z', 'y', - 1, - 1, node, xyz, 0 ); // px
	buildPlane( 'z', 'y',   1, - 1, node, xyz, 1 ); // nx
	buildPlane( 'x', 'z',   1,   1, node, xyz, 2 ); // py
	buildPlane( 'x', 'z',   1, - 1, node, xyz, 3 ); // ny
	buildPlane( 'x', 'y',   1, - 1, node, xyz, 4 ); // pz
	buildPlane( 'x', 'y', - 1, - 1, node, xyz, 5 ); // nz
	
	function buildPlane( u, v, udir, vdir, node, direction, materialIndex ) 
	{
		var w, ix, iy,
		gridX = scope.widthSegments,
		gridY = scope.heightSegments,
		offset = scope.vertices.length;

		if ( ( u === 'x' && v === 'y' ) || ( u === 'y' && v === 'x' ) ) {

			w = 'z';

		} else if ( ( u === 'x' && v === 'z' ) || ( u === 'z' && v === 'x' ) ) {

			w = 'y';
			gridY = scope.depthSegments;

		} else if ( ( u === 'z' && v === 'y' ) || ( u === 'y' && v === 'z' ) ) {

			w = 'x';
			gridX = scope.depthSegments;
		}
		
		var gridX1 = gridX + 1,
		gridY1 = gridY + 1,
		normal = new THREE.Vector3();
		
		var depth = (materialIndex & 1);

		normal[ w ] = depth == 0 ? 1 : - 1;
		
		var xSeg = scope.widthSegments+1;
		var ySeg = scope.heightSegments+1;
		var zSeg = scope.depthSegments+1;
		if (materialIndex == 0)
		{
			for ( iy = 0; iy < gridY1; iy ++ ) 
			{
				//var index1 = xSeg*ySeg*(zSeg-1)+xSeg*(iy+1);
				var index1 = xSeg*ySeg*zSeg-(iy*xSeg);
				for ( ix = 0; ix < gridX1; ix ++ ) 
				{
					var vector = new THREE.Vector3();
					vector['x'] = node[index1-1][0];
					vector['y'] = node[index1-1][1];
					vector['z'] = node[index1-1][2];

					scope.vertices.push( vector );
					index1 = index1 - xSeg*ySeg;
				}
			}
		}
		else if (materialIndex == 1)
		{	        
			for ( iy = 0; iy < gridY1; iy ++ ) 
			{
				//var index1 = xSeg*iy+1;
				var index1 = xSeg*(ySeg-1) -(xSeg*iy) +1;
				for ( ix = 0; ix < gridX1; ix ++ ) 
				{
					var vector = new THREE.Vector3();
					vector['x'] = node[index1-1][0];
					vector['y'] = node[index1-1][1];
					vector['z'] = node[index1-1][2];

					scope.vertices.push( vector );
					index1 = index1 + xSeg*ySeg;
				}
			}
		}
		else if (materialIndex == 2)
		{
			for ( iy = 0; iy < gridY1; iy ++ ) 
			{
				//var index1 = xSeg*ySeg*iy + 1;
				var index1 = (xSeg)*(ySeg-1) + xSeg*ySeg*iy + 1;
				for ( ix = 0; ix < gridX1; ix ++ ) 
				{
					var vector = new THREE.Vector3();
					vector['x'] = node[index1-1][0];
					vector['y'] = node[index1-1][1];
					vector['z'] = node[index1-1][2];

					scope.vertices.push( vector );
					index1 = index1 + 1;
				}
			}              
		}
		else if (materialIndex == 3)
		{
			for ( iy = 0; iy < gridY1; iy ++ ) 
			{
				//var index1 = xSeg*ySeg*(zSeg-iy) - xSeg + 1;
				var index1 = xSeg*ySeg*(zSeg-iy-1)+1;
				for ( ix = 0; ix < gridX1; ix ++ ) 
				{
					var vector = new THREE.Vector3();
					vector['x'] = node[index1-1][0];
					vector['y'] = node[index1-1][1];
					vector['z'] = node[index1-1][2];

					scope.vertices.push( vector );
					index1 = index1 + 1;
				}
			}
		}
		else if (materialIndex == 4)
		{
			for ( iy = 0; iy < gridY1; iy ++ ) 
			{
				//var index1 = xSeg*ySeg*(zSeg-1) + iy*xSeg+1;
				var index1 = xSeg*ySeg*(zSeg-1) + ((ySeg-iy-1))*xSeg +1;
				for ( ix = 0; ix < gridX1; ix ++ ) 
				{
					var vector = new THREE.Vector3();
					vector['x'] = node[index1-1][0];
					vector['y'] = node[index1-1][1];
					vector['z'] = node[index1-1][2];

					scope.vertices.push( vector );
					index1 = index1 + 1;
				}
			}
		}
		else
		{
			for ( iy = 0; iy < gridY1; iy ++ ) 
			{
				//var index1 = (iy+1)*xSeg;
				var index1 = (ySeg-iy)*xSeg;
				for ( ix = 0; ix < gridX1; ix ++ ) 
				{
					var vector = new THREE.Vector3();
					vector['x'] = node[index1-1][0];
					vector['y'] = node[index1-1][1];
					vector['z'] = node[index1-1][2];

					scope.vertices.push( vector );
					index1 = index1 - 1;
				}
			}
		}

		for ( iy = 0; iy < gridY; iy ++ ) {

			for ( ix = 0; ix < gridX; ix ++ ) {

				var a = ix + gridX1 * iy;
				var b = ix + gridX1 * ( iy + 1 );
				var c = ( ix + 1 ) + gridX1 * ( iy + 1 );
				var d = ( ix + 1 ) + gridX1 * iy;

				var uva = new THREE.Vector2( ix / gridX, 1 - iy / gridY );
				var uvb = new THREE.Vector2( ix / gridX, 1 - ( iy + 1 ) / gridY );
				var uvc = new THREE.Vector2( ( ix + 1 ) / gridX, 1 - ( iy + 1 ) / gridY );
				var uvd = new THREE.Vector2( ( ix + 1 ) / gridX, 1 - iy / gridY );

				var face;
				if(direction >= 0)
					face = new THREE.Face3( a + offset, b + offset, d + offset );
				else
					face = new THREE.Face3( d + offset, b + offset, a + offset );
				face.materialIndex = materialIndex;

				scope.faces.push( face );
				
				if(direction >= 0)
					face = new THREE.Face3( b + offset, c + offset, d + offset );
				else
					face = new THREE.Face3( d + offset, c + offset, b + offset );
				face.materialIndex = materialIndex;

				scope.faces.push( face );
			}
		}
	}
	
	this.mergeVertices();
	
	scope.verticesNeedUpdate = true;
	scope.normalsNeedUpdate = true;
	scope.computeBoundingSphere();
	scope.computeFaceNormals();
	//scope.computeVertexNormals();
};

THREE.SGridGeometry.prototype = Object.create( THREE.Geometry.prototype );
THREE.SGridGeometry.prototype.constructor = THREE.SGridGeometry;

function MakeCustomLut(customcolor)
{
	lut = new THREE.Lut( 'rainbow', 256 );

	var lutMax = -Number.MAX_VALUE;
	var lutMin = Number.MAX_VALUE;
	for(var i = customcolor.length-1 ; i >= 0 ; i--)
	{
		lutMax = Math.max(lutMax, customcolor[i][0]);
		lutMin = Math.min(lutMin, customcolor[i][0]);
	}

	lutLength = lutMax-lutMin;
	arrayOfColors = []
	for(var i = 0 ; i < customcolor.length ; i++)
	{
		scalarValue = customcolor[i][0];

		scalarColor = customcolor[i][2];
		scalarColor = colourNameToHex(scalarColor);
		scalarColor = scalarColor.replace("#", "0x");

		arrayOfColors.push([(scalarValue-lutMin)/lutLength, scalarColor])
	}
	//  "grayscale" : [ [ 0.0, '0x000000' ], [ 0.2, '0x404040' ], [ 0.5, '0x7F7F80' ], [ 0.8, '0xBFBFBF' ],  [ 1.0, '0xFFFFFF' ] ]
	lut.addColorMap("custom", arrayOfColors);
	lut.changeNumberOfColors(customcolor.length);
	lut = lut.changeColorMap("custom");
	lut.setMax( lutMax );
	lut.setMin( lutMin );
}

function linspace(a,b,n)
{
     if(typeof n === "undefined")
     	n = Math.max(Math.round(b-a)+1,1);
     if(n<2)
     {
     	return n===1?[a]:[];
     }
     var i,ret = Array(n);
     n--;
     for(i=n;i>=0;i--)
     {
     	ret[i] = (i*b+(n-i)*a)/n;
     }

	return ret;
}


function MakeLut(scalar)
{
    var colorMap;
	var numberOfColors;
	colorMap = 'rainbow';
	numberOfColors = 256;//512
	var lutColors = [];
	
	lut = new THREE.Lut( colorMap, numberOfColors );
	
	var lutMax = -Number.MAX_VALUE;
	var lutMin = Number.MAX_VALUE;
	for(var i = scalar.length-1 ; i >= 0 ; i--)
	{
		lutMax = Math.max(lutMax, scalar[i]);
		lutMin = Math.min(lutMin, scalar[i]);
	}

	bin = linspace(Math.floor(lutMin), Math.ceil(lutMax), 256);

	var hist = histogram({
    	data : scalar,
        bins : bin
    });

	totalItem = scalar.length;
	count = 0;
    for(var i = 0 ; i < hist.length ; i++)
    {
    	count = count + hist[i].y;
    	if(count/totalItem *100 > 98)
    	{
    		lutMax = hist[i].x;
    		break;
    	}
    }

    count = 0;
    for(var i = hist.length-1 ; i >= 0 ; i--)
    {
    	count = count + hist[i].y;
    	if(count/totalItem *100 > 98)
    	{
    		lutMin = hist[i].x;
    		break;
    	}
    }

    lut.setMax( lutMax );
	lut.setMin( lutMin );

	CreateLutLegend(lut);
}


function SetGridFaceColor(model, scalar)
{
	var xSeg = model.geometry.widthSegments;
	var ySeg = model.geometry.heightSegments;
	var zSeg = model.geometry.depthSegments;
	
	for(var i =0 ; i < model.geometry.faces.length ; i++)	
    {
    	var face = model.geometry.faces[i];
    	var scalarIndex;
    	if(face.materialIndex == 0)
    	{
    		var quardIndex = parseInt(i/2);
    		var xIndex = xSeg;
    		var yIndex = ySeg - parseInt(quardIndex/zSeg);
    		var zIndex = zSeg - parseInt(quardIndex%zSeg);
    		
    		scalarIndex = (zIndex-1)*(xSeg*ySeg)+(yIndex-1)*xSeg + xIndex - 1;
    	}
    	else if(face.materialIndex == 1)
    	{
    		var tempi = i - (ySeg*zSeg*2);
    		var quardIndex = parseInt(tempi/2);
    		
    		var xIndex = 0;
    		var yIndex = 0 + parseInt(quardIndex/zSeg);
    		var zIndex = 0 + parseInt(quardIndex%zSeg);
    		
    		scalarIndex = (zIndex)*(xSeg*ySeg)+(ySeg-yIndex-1)*xSeg + xIndex;
    	}
    	else if(face.materialIndex == 2)
    	{
    		var tempi = i - (ySeg*zSeg*2)*2;
    		var quardIndex = parseInt(tempi/2);
    		
    		var xIndex = 0 + parseInt(quardIndex%xSeg);
    		var yIndex = ySeg;
    		var zIndex = 0 + parseInt(quardIndex/xSeg);

    		scalarIndex = (zIndex)*(xSeg*ySeg)+(ySeg-1)*xSeg + xIndex;
    	}
    	else if(face.materialIndex == 3)
    	{
    		var tempi = i - (ySeg*zSeg*2)*2 - (xSeg*zSeg*2);
    		var quardIndex = parseInt(tempi/2);
    		
    		var xIndex = 0 + parseInt(quardIndex%xSeg);
    		var yIndex = 0;
    		var zIndex = zSeg - parseInt(quardIndex/xSeg);
    		
    		scalarIndex = (zIndex-1)*(xSeg*ySeg) + xIndex;
    	}
    	else if(face.materialIndex == 4)
    	{
    		var tempi = i - (ySeg*zSeg*2)*2 - (xSeg*zSeg*2)*2;
    		var quardIndex = parseInt(tempi/2);
    		
    		var xIndex = 0 + parseInt(quardIndex%xSeg);
    		var yIndex = ySeg - parseInt(quardIndex/xSeg);
    		var zIndex = zSeg;
    		
    		scalarIndex = (zIndex-1)*(xSeg*ySeg)+(yIndex-1)*xSeg + xIndex;
    	}
    	else
    	{
    		var tempi = i - (ySeg*zSeg*2)*2 - (xSeg*zSeg*2)*2 - (xSeg*ySeg*2);
    		var quardIndex = parseInt(tempi/2);
    		
    		var xIndex = xSeg - parseInt(quardIndex%xSeg);
    		var yIndex = ySeg - parseInt(quardIndex/xSeg)
    		var zIndex = 0;
    		
    		scalarIndex = (yIndex-1)*xSeg + xIndex-1;
    	}

		color = lut.getColor( scalar[scalarIndex] );
		face.color = color;
    }
}

function GetLut()
{
	return lut;
}


function CreateSectionCube(model, axis, offset)
{
	var xSeg = model.geometry.widthSegments;
	var ySeg = model.geometry.heightSegments;
	var zSeg = model.geometry.depthSegments;
	var planeIndex = (ySeg+1)*(zSeg+1);
	var xIndex = planeIndex*2 + (xSeg-1)*(zSeg+1) + (zSeg*(xSeg-1)) ;
	
	var Point1 = model.geometry.vertices[0];
	var Point2 = model.geometry.vertices[zSeg];
	var Point3 = model.geometry.vertices[ySeg*(zSeg+1)];
	var Point4 = model.geometry.vertices[ySeg*(zSeg+1)+zSeg];
	var Point5 = model.geometry.vertices[planeIndex+zSeg];
	var Point6 = model.geometry.vertices[planeIndex];
	var Point7 = model.geometry.vertices[planeIndex+ySeg*(zSeg+1)+zSeg];
	var Point8 = model.geometry.vertices[planeIndex+ySeg*(zSeg+1)];
	
	var xVector = new THREE.Vector3(); 
	xVector.subVectors(Point4, Point8).normalize();
	var yVector = new THREE.Vector3();
	yVector.subVectors(Point6, Point8).normalize();
	var zVector = new THREE.Vector3();
	zVector.subVectors(Point7, Point8).normalize();
	
	var origin = new THREE.Vector3();
	origin = Point8;
	
	zInterval = [];
	zValue = 0;
	for(var i = 0 ; i < zSeg ; i++)
	{
		var temp = new THREE.Vector3();
		var length = temp.subVectors(model.geometry.vertices[planeIndex+ySeg*(zSeg+1)+i+1], model.geometry.vertices[planeIndex+ySeg*(zSeg+1)+i]).length();
		zValue += length
		zInterval.push(zValue);
	}
	
	yInterval = [];
	yValue = 0;
	for(var i = 0 ; i < ySeg ; i++)
	{
		var temp = new THREE.Vector3();
		var point1 = model.geometry.vertices[planeIndex+(ySeg-i)*(zSeg+1)];
		var point2 = model.geometry.vertices[planeIndex+(ySeg-i-1)*(zSeg+1)];
		var length = temp.subVectors(point2, point1).length();
		yValue += length
		yInterval.push(yValue);
	}

	var xPoint = [];
	xPoint.push(model.geometry.vertices[planeIndex+ySeg*(zSeg+1)]);
	for(var i = 0 ; i < xSeg -1 ; i++)
	{
		xPoint.push(model.geometry.vertices[xIndex+i]);
	}
	xPoint.push(model.geometry.vertices[ySeg*(zSeg+1)+zSeg]);
	
	xInterval = []
	xValue =0;
	for(var i = 0 ; i < xSeg ; i++)
	{
		var temp = new THREE.Vector3();
		var length = temp.subVectors(xPoint[i+1], xPoint[i]).length();
		xValue += length;
		xInterval.push(xValue);
	}
	
	var nodes = [];
	if(axis == "X")
	{
		for(var z = 0 ; z <= zInterval.length ; z++)
		{
			for(var y = 0 ; y <= yInterval.length ; y++)
			{
				for(var x = offset ; x <= offset+1 ; x++)
				{
					var node = new THREE.Vector3();
					node.copy(origin);
					if(z != 0)	
					{
						var temp = new THREE.Vector3();
						temp.copy(zVector);
						temp.multiplyScalar(zInterval[z-1]);
						node.addVectors(node, temp);
					}
					if(y != 0)
					{
						var temp = new THREE.Vector3();
						temp.copy(yVector);
						temp.multiplyScalar(yInterval[y-1]);
						node.addVectors(node, temp);
					}
					if(x != 0)
					{
						var temp = new THREE.Vector3();
						temp.copy(xVector);
						temp.multiplyScalar(xInterval[x-1]);
						node.addVectors(node, temp);
					}
					nodes.push([node.x, node.y, node.z]);
				}	
			}		
		}
		
		var cubeGeometry = new THREE.SGridGeometry(1, ySeg, zSeg, nodes);
		return cubeGeometry;
	}
	else if (axis == "Y")
	{
		for(var z = 0 ; z <= zInterval.length ; z++)
		{
			for(var y = offset ; y <= offset+1 ; y++)
			{
				for(var x = 0 ; x <= xInterval.length ; x++)
				{
					var node = new THREE.Vector3();
					node.copy(origin);
					if(z != 0)	
					{
						var temp = new THREE.Vector3();
						temp.copy(zVector);
						temp.multiplyScalar(zInterval[z-1]);
						node.addVectors(node, temp);
					}
					if(y != 0)
					{
						var temp = new THREE.Vector3();
						temp.copy(yVector);
						temp.multiplyScalar(yInterval[y-1]);
						node.addVectors(node, temp);
					}
					if(x != 0)
					{
						var temp = new THREE.Vector3();
						temp.copy(xVector);
						temp.multiplyScalar(xInterval[x-1]);
						node.addVectors(node, temp);
					}
					nodes.push([node.x, node.y, node.z]);
				}	
			}		
		}
		
		var cubeGeometry = new THREE.SGridGeometry(xSeg, 1, zSeg, nodes);
		return cubeGeometry;
	}
	else if (axis == "Z")
	{
		for(var z = offset ; z <= offset+1 ; z++)
		{
			for(var y = 0 ; y <= yInterval.length ; y++)
			{
				for(var x = 0 ; x <= xInterval.length ; x++)
				{
					var node = new THREE.Vector3();
					node.copy(origin);
					
					
					if(z != 0)	
					{
						var temp = new THREE.Vector3();
						temp.copy(zVector);
						temp.multiplyScalar(zInterval[z-1]);
						node.addVectors(node, temp);
					}
					if(y != 0)
					{
						var temp = new THREE.Vector3();
						temp.copy(yVector);
						temp.multiplyScalar(yInterval[y-1]);
						node.addVectors(node, temp);
					}
					if(x != 0)
					{
						var temp = new THREE.Vector3();
						temp.copy(xVector);
						temp.multiplyScalar(xInterval[x-1]);
						node.addVectors(node, temp);
					}
					
					nodes.push([node.x, node.y, node.z]);
				}	
			}		
		}
		
		var cubeGeometry = new THREE.SGridGeometry(xSeg, ySeg, 1, nodes);
		
		return cubeGeometry;
	}
}



function CreateModelBound(model, hex)
{
	var xSeg = model.geometry.widthSegments;
	var ySeg = model.geometry.heightSegments;
	var zSeg = model.geometry.depthSegments;
	var planeIndex = (ySeg+1)*(zSeg+1);
	
	var Point1 = model.geometry.vertices[0];
	var Point2 = model.geometry.vertices[zSeg];
	var Point3 = model.geometry.vertices[ySeg*(zSeg+1)];
	var Point4 = model.geometry.vertices[ySeg*(zSeg+1)+zSeg];
	var Point5 = model.geometry.vertices[planeIndex+zSeg];
	var Point6 = model.geometry.vertices[planeIndex];
	var Point7 = model.geometry.vertices[planeIndex+ySeg*(zSeg+1)+zSeg];
	var Point8 = model.geometry.vertices[planeIndex+ySeg*(zSeg+1)];
	
	var object = new THREE.Object3D();
	var material = new THREE.LineBasicMaterial({
        color: hex
    });

	var geometry = new THREE.Geometry();
	geometry.vertices.push(Point1);
	geometry.vertices.push(Point2);
	geometry.vertices.push(Point4);
	geometry.vertices.push(Point3);
	geometry.vertices.push(Point1);
	var line = new THREE.Line(geometry, material);    
	object.add(line);
	
	geometry = new THREE.Geometry();
	geometry.vertices.push(Point5);
	geometry.vertices.push(Point6);
	geometry.vertices.push(Point8);
	geometry.vertices.push(Point7);
	geometry.vertices.push(Point5);
	line = new THREE.Line(geometry, material);    
	object.add(line);
	
	geometry = new THREE.Geometry();
	geometry.vertices.push(Point1);
	geometry.vertices.push(Point5);
	line = new THREE.Line(geometry, material); 
	object.add(line);
	
	geometry = new THREE.Geometry();
	geometry.vertices.push(Point2);
	geometry.vertices.push(Point6);
	line = new THREE.Line(geometry, material); 
	object.add(line);
		
	geometry = new THREE.Geometry();
	geometry.vertices.push(Point3);
	geometry.vertices.push(Point7);
	line = new THREE.Line(geometry, material); 
	object.add(line);
	
	geometry = new THREE.Geometry();
	geometry.vertices.push(Point4);
	geometry.vertices.push(Point8);
	line = new THREE.Line(geometry, material); 
	object.add(line);

	return object;  
}
