var glContainer = document.getElementById( 'ModelView' );
var inset = document.getElementById('inset');
var scalarview = document.getElementById('scalarView');
var scene;
var camera;
var renderer;
var controls;	
var light;

var camera2, scene2, renderer2;
var camera3, scene3, renderer3;

var propertyList = [];

function init()
{
	scene = new THREE.Scene();
	canvasWidth = glContainer.offsetWidth;
    canvasHeight = glContainer.offsetHeight;
	//canvasWidth = window.innerWidth;
	//canvasHeight = window.innerHeight;
	
	renderer = new THREE.WebGLRenderer();
	renderer.setSize( canvasWidth, canvasHeight);
	glContainer.appendChild( renderer.domElement );

	camera = new THREE.PerspectiveCamera(45, canvasWidth / canvasHeight, 1, 80000);
	camera.position.set(0, 00, 220); 
	camera.lookAt(scene.position); 	
	
	scene.add(camera); 
	
	//light = new THREE.DirectionalLight(0xffffff);
    light = new THREE.PointLight(0xffffff, 1.0);
    light.position.copy( camera.position );
    scene.add(light);
    
    //scene.add( new THREE.AmbientLight( 0xee44ee ) );
    
    ResetControls(scene.position);

	renderer2 = new THREE.WebGLRenderer({ alpha: true });
	//renderer2.setClearColor( 0xffffff, 0 );
    renderer2.setSize( 140, 140 );
    inset.appendChild( renderer2.domElement );
    scene2 = new THREE.Scene();
    camera2 = new THREE.PerspectiveCamera( 45, 1, 1, 80000 );
    camera2.position.set(0, 00, 18);
	camera2.lookAt(scene2.position);
    scene2.add( camera2 );

	scene2.add( makeCoordinateArrows() );

	renderer3 = new THREE.WebGLRenderer({ alpha: true });//
	renderer3.setSize( 500, 100 );
	//renderer3.setClearColor(0xff0000, 1)

	scalarview.appendChild(renderer3.domElement);
	scene3 = new THREE.Scene();
	camera3 = new THREE.PerspectiveCamera( 45, 5, 1, 80000 );
	camera3.position.set(0, 0, 1);
	camera3.lookAt(scene3.position);
    scene3.add( camera3 );
/*
    var geom = new THREE.Geometry();
	var v1 = new THREE.Vector3(-250,-250,0);
	var v2 = new THREE.Vector3(250,-250,0);
	var v3 = new THREE.Vector3(250,250,0);
	var v4 = new THREE.Vector3(-250, 250, 0);

	geom.vertices.push(v4);
	geom.vertices.push(v3);
	geom.vertices.push(v1);
	geom.vertices.push(v2);

	geom.faces.push( new THREE.Face3( 0, 2, 1 ) );
	geom.faces.push( new THREE.Face3( 2, 3, 1 ) );


	var texture = THREE.ImageUtils.loadTexture( '/image/Geo_Map.png' );

	var material = new THREE.MeshLambertMaterial({map:texture});
	var plane = new THREE.Mesh(geom, material);

		geom.computeBoundingBox();
    geom.verticesNeedUpdate = true;
    geom.normalsNeedUpdate = true;
	geom.computeBoundingSphere();
    geom.computeFaceNormals();

    	var uvs = geom.faceVertexUvs[ 0 ];
	var uv1 = [
                    new THREE.Vector2(0,0),
                    new THREE.Vector2(0,1),
                    new THREE.Vector2(1,0)
                ];
	var uv2 = [
                    new THREE.Vector2(0,1),
                    new THREE.Vector2(1,1),
                    new THREE.Vector2(1,0)
                ];
    uvs.push(uv1);
      uvs.push(uv2);
	scene.add(plane);
*/

    render();
}

function makeCoordinateArrows() {
    var coordinateArrows = new THREE.Object3D();
    var org = new THREE.Vector3( 0, 0, 0);

    var dir = new THREE.Vector3( 0, 0, 1 );
    coordinateArrows.add( new THREE.ArrowHelper( dir, org, 8, 0x0000FF ) ); // Blue = z
    dir = new THREE.Vector3( 0, 1, 0 );
    coordinateArrows.add( new THREE.ArrowHelper( dir, org, 8, 0x00FF00 ) ); // Green = y
    dir = new THREE.Vector3( 1, 0, 0 );
    coordinateArrows.add( new THREE.ArrowHelper( dir, org, 8, 0xFF0000 ) ); // Red = x

    return coordinateArrows;
}

function SetProperty(cellData)
{
	propertyList.length = 0;
	propertyList = cellData;
}

function ResetControls(target)
{
	controls = new THREE.TrackballControls(camera, renderer.domElement);
	controls.rotateSpeed = 2.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;

	controls.noZoom = false;
	controls.noPan = false;

	controls.staticMoving = true;
	controls.dynamicDampingFactor = 0.3;
	
	if(target != null)
		controls.target = target;
	controls.addEventListener( 'change', render );
}

function animate(){
	requestAnimationFrame( animate );
	controls.update();

	camera2.position.copy( camera.position );
    camera2.position.sub( controls.target );
	camera2.position.setLength( 18 );
    camera2.lookAt( scene2.position );
}

function render(){
	light.position.copy( camera.position );
	renderer.render( scene, camera );
	renderer2.render(scene2, camera2);
	renderer3.render(scene3, camera3);
}

function onWindowResize()
{
	var canvasWidth = glContainer.offsetWidth;
    var canvasHeight = glContainer.offsetHeight;
	camera.aspect = canvasWidth / canvasHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( canvasWidth, canvasHeight );

    render();
}

function ClearLegendView()
{
	var sceneChild = scene3.children;

	for(var i = sceneChild.length-1 ; i >= 0 ; i--)
	{
		if(sceneChild[i].type == "PerspectiveCamera")
			continue;

		var child = sceneChild[i]
		scene3.remove(child);
		child.geometry.dispose();
		child.material.dispose();
	}
}

function CreateLutLegend(lut)
{
	ClearLegendView();
	var legend = lut.setLegendOn( { 'layout':'horizontal', 'position': { 'x': 0, 'y': 0.13, 'z': 0 } } );

	scene3.add ( legend );

	var labels = lut.setLegendLabels( { 'ticks': 5 } );
	for ( var i = 0; i < Object.keys( labels[ 'ticks' ] ).length; i++ )
	{
		scene3.add ( labels[ 'ticks' ][ i ] );
		scene3.add ( labels[ 'lines' ][ i ] );
	}

	render();
}

function ModelBoundary(min, max, boundingBox)
{
	min.x = Math.min(min.x, boundingBox.min.x);
	min.y = Math.min(min.y, boundingBox.min.y);
	min.z = Math.min(min.z, boundingBox.min.z);
	max.x = Math.max(max.x, boundingBox.max.x);
	max.y = Math.max(max.y, boundingBox.max.y);
	max.z = Math.max(max.z, boundingBox.max.z);
}

function ReCreateSubModel(model)
{
	var subChild = model.children;
	var newMeshes = [];
	for(var j = subChild.length-1 ; j>=0 ; j--)
    {
    	var oldMaterialSide = subChild[j].material.side;
		var oldMaterialWire = subChild[j].material.wireframe;

		if(model.name == "Universe")
		{

				var newMaterial = new THREE.MeshBasicMaterial({color:0xffffff, side:oldMaterialSide, wireframe:true});
				subChild[j].material = newMaterial;

		}
		else
		{
			if(oldMaterialWire )
			{
				subChild[j].name = "wireframe";
				var newMaterial = new THREE.MeshLambertMaterial({color:0xffffff, side:oldMaterialSide, wireframe:false, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0});
				var newMesh = new THREE.Mesh(subChild[j].geometry, newMaterial);
				newMeshes.push(newMesh);
			}
			else
			{
				subChild[j].material.polygonOffset = true;
				subChild[j].material.polygonOffsetFactor= 1.0;
				subChild[j].material.polygonOffsetUnits= 4.0;
				var newMaterial = new THREE.MeshBasicMaterial({color:0xffffff, side:oldMaterialSide, wireframe:true});
				var newMesh = new THREE.Mesh(subChild[j].geometry, newMaterial);
				newMesh.name = "wireframe";
				newMeshes.push(newMesh);
			}
		}
    }

    for(var i = newMeshes.length-1 ; i >= 0 ; i--)
    {
    	model.add(newMeshes[i]);
    }

   	edges = new THREE.ChildEdgesGeometry(model, 0x00ff00 );
	edges.name = "edge";
	model.add(edges);
}

function AddModel(data, func)
{
	var objectLoader = new THREE.ObjectLoader();
	objectLoader.parse(data, function (e) {
    	var children = e.children;
    	
    	var model3D = new THREE.Object3D();
    	model3D.name = e.name;
    	model3D.userData = {"objectType":"Model3D"}
    	
    	for(var i= children.length-1 ; i >= 0 ; i--)
    	{    		
    		if(children[i].children.length == 0)
    		{
    			children[i].geometry.computeBoundingBox();
    		}
    		else
    		{
    			var subChild = children[i].children;
    			var aabbMin = new THREE.Vector3(Number.MAX_VALUE, Number.MAX_VALUE, Number.MAX_VALUE);
    		    var aabbMax = new THREE.Vector3(-Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE);
    			for(var j = subChild.length-1 ; j>=0 ; j--)
    			{
    				subChild[j].geometry.computeBoundingBox();
    				subChild[j].geometry.verticesNeedUpdate = true;
    				subChild[j].geometry.normalsNeedUpdate = true;
					subChild[j].geometry.computeBoundingSphere();
    				subChild[j].geometry.computeFaceNormals();
    				subChild[j].geometry.computeVertexNormals();
    				ModelBoundary(aabbMin, aabbMax, subChild[j].geometry.boundingBox);
    			}
    			ReCreateSubModel(children[i]);
    			children[i].userData = {'Model':true, 'edge':true, 'wireframe':true};
    			children[i].geometry.boundingBox = new THREE.Box3(aabbMin, aabbMax);
    		}

    		model3D.add(children[i]);
    	}
    	scene.add(model3D);
    	render();
    	func(model3D);
    }, '.'); 
}

function AddImage(voxetData, func)
{

	var origin = new THREE.Vector3( voxetData.Voxetdata.Origin[0], voxetData.Voxetdata.Origin[1], voxetData.Voxetdata.Origin[2] );
	var nx = voxetData.Voxetdata.DIR_NUM[0];
	var ny = voxetData.Voxetdata.DIR_NUM[1];
	var nz = voxetData.Voxetdata.DIR_NUM[2];

	var xSize= voxetData.Voxetdata.U[0];
	var ySize = voxetData.Voxetdata.V[1];
	var zSize = voxetData.Voxetdata.W[2];

    var geom = new THREE.Geometry();
	var v1 = new THREE.Vector3(0,0,0);
	var v2 = new THREE.Vector3(voxetData.Voxetdata.U[0], voxetData.Voxetdata.U[1], voxetData.Voxetdata.U[2]);
	var v3 = new THREE.Vector3(voxetData.Voxetdata.U[0]+voxetData.Voxetdata.V[0], voxetData.Voxetdata.U[1]+voxetData.Voxetdata.V[1], voxetData.Voxetdata.U[2]+voxetData.Voxetdata.V[2]);
	var v4 = new THREE.Vector3(voxetData.Voxetdata.V[0], voxetData.Voxetdata.V[1], voxetData.Voxetdata.V[2]);

	geom.vertices.push(v4);
	geom.vertices.push(v3);
	geom.vertices.push(v1);
	geom.vertices.push(v2);

	geom.faces.push( new THREE.Face3( 0, 2, 1 ) );
	geom.faces.push( new THREE.Face3( 2, 3, 1 ) );

	geom.computeBoundingBox();
    geom.verticesNeedUpdate = true;
    geom.normalsNeedUpdate = true;
	geom.computeBoundingSphere();
    geom.computeFaceNormals();

    var uvs = geom.faceVertexUvs[ 0 ];
	var uv1 = [
                    new THREE.Vector2(0,0),
                    new THREE.Vector2(0,1),
                    new THREE.Vector2(1,0)
                ];
	var uv2 = [
                    new THREE.Vector2(0,1),
                    new THREE.Vector2(1,1),
                    new THREE.Vector2(1,0)
                ];
    uvs.push(uv1);
    uvs.push(uv2);


	var texture = THREE.ImageUtils.loadTexture( '/image/' + voxetData.ImageFile );

	var planeMaterial = new THREE.MeshLambertMaterial({ map : texture, side:THREE.DoubleSide });
	var plane = new THREE.Mesh(geom, planeMaterial);

	plane.name = voxetData.Voxetdata.Name;

	plane.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(origin.x, origin.y, origin.z) );

	plane.userData = {'Model':true, 'edge':true, 'wireframe':true};

	scene.add(plane);

	render();

	func(plane);
}

function AddVoxet(voxetData, func)
{
	var origin = new THREE.Vector3( voxetData.Voxetdata.Origin[0], voxetData.Voxetdata.Origin[1], voxetData.Voxetdata.Origin[2] );
	var nx = voxetData.Voxetdata.DIR_NUM[0];
	var ny = voxetData.Voxetdata.DIR_NUM[1];
	var nz = voxetData.Voxetdata.DIR_NUM[2];

	var nodes = [];
	var index = 0;
	for(var z = 0 ; z <= nz ; z++)
	{
		for(var y = 0 ; y<= ny ; y++)
		{
			for(var x = 0 ; x <= nx ; x++)
			{
				var xValue = voxetData.Voxetdata.U[0]*x + voxetData.Voxetdata.V[0]*y + voxetData.Voxetdata.W[0]*z;
				var yValue = voxetData.Voxetdata.U[1]*x + voxetData.Voxetdata.V[1]*y + voxetData.Voxetdata.W[1]*z;
				var zValue = voxetData.Voxetdata.U[2]*x + voxetData.Voxetdata.V[2]*y + voxetData.Voxetdata.W[2]*z;
				nodes[index] = [xValue, yValue, zValue];
				index++;
			}
		}
	}

	var cubeGeometry = new THREE.SGridGeometry(nx, ny, nz, nodes);
	var voxet = new THREE.Mesh( cubeGeometry, new THREE.MeshLambertMaterial( { color: 0x0000ff, side: THREE.DoubleSide, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0}) );
	voxet.geometry.computeBoundingBox();
	voxet.name = voxetData.Voxetdata.Name;

	var wireSGrid = new THREE.SGridWireGeometry(nx, ny, nz, nodes, 0xffffff);
	//var wireSGrid = new THREE.Mesh(cubeGeometry, new THREE.MeshLambertMaterial({color:0xffffff, side: THREE.DoubleSide, wireframe: true}))
	wireSGrid.name = 'wireframe';
	voxet.add(wireSGrid);

	//var edges = CreateModelBound(sGrid, 0x00ff00);
	edges = new THREE.EdgesGeometry( voxet, 0x00ff00 );
	edges.name = "edge";
	voxet.add(edges);

	voxet.userData = {'Model':true, 'edge':true, 'wireframe':true};
	scene.add(voxet);
	render();

	func(voxet, voxetData.PROPERTY, voxetData.REGION);
}
/*
function AddVoxet(voxetData, func)
{
	var origin = new THREE.Vector3( voxetData.Voxetdata.Origin[0], voxetData.Voxetdata.Origin[1], voxetData.Voxetdata.Origin[2] );
	var nx = voxetData.Voxetdata.DIR_NUM[0];
	var ny = voxetData.Voxetdata.DIR_NUM[1];
	var nz = voxetData.Voxetdata.DIR_NUM[2];
	
	var xSize= voxetData.Voxetdata.U[0]*nx;
	var ySize = voxetData.Voxetdata.V[1]*ny;
	var zSize = voxetData.Voxetdata.W[2]*nz;
	
	var x = Math.abs(xSize);
	var y = Math.abs(ySize);
	var z = Math.abs(zSize);
	
	var xDir = xSize >= 0 ? 1 : -1;
	var yDir = ySize >= 0 ? 1 : -1;
	var zDir = zSize >= 0 ? 1 : -1;

	var voxetGeometry = new THREE.VoxetGeometry(x, y, z, xDir, yDir, zDir, nx, ny, nz);
	var voxet = new THREE.Mesh( voxetGeometry, new THREE.MeshLambertMaterial( { color: 0xeeeeee, side: THREE.DoubleSide, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0}) );
	voxet.geometry.computeBoundingBox();
	voxet.name = voxetData.Voxetdata.Name;
	
	voxet.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(origin.x, origin.y, origin.z) );

	var wireSVoxet = new THREE.VoxetWireGeometry(voxetData.Voxetdata.U[0],voxetData.Voxetdata.V[1], voxetData.Voxetdata.W[2], nx, ny, nz, 0xffffff);
	//var wireSVoxet = new THREE.Mesh(voxetGeometry, new THREE.MeshBasicMaterial({color:0xffffff, side: THREE.DoubleSide, wireframe: true}))
	wireSVoxet.name = 'wireframe';
	wireSVoxet.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(origin.x, origin.y, origin.z) );
	voxet.add(wireSVoxet);

	//var edges = CreateModelBound(voxet, 0x00ff00);
	edges = new THREE.EdgesGeometry( voxet, 0x00ff00 );
	edges.name = "edge";
	voxet.add(edges);

	voxet.userData = {'Model':true, 'edge':true, 'wireframe':true};
	scene.add(voxet);
	render();
	
	func(voxet, voxetData.PROPERTY, voxetData.REGION);
}
*/
function AddSGrid(sgridData, func)
{
	var name = sgridData.SGriddata.Name;
	
	var nx = sgridData.SGriddata.DIR_NUM[0]-1;
	var ny = sgridData.SGriddata.DIR_NUM[1]-1;
	var nz = sgridData.SGriddata.DIR_NUM[2]-1;
	
	var nodes = sgridData.Node;
	
	var cubeGeometry = new THREE.SGridGeometry(nx, ny, nz, nodes);
	var sGrid = new THREE.Mesh( cubeGeometry, new THREE.MeshLambertMaterial( { color: 0x0000ff, side: THREE.DoubleSide, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0}) );
	sGrid.geometry.computeBoundingBox();
	sGrid.name = name;

	var wireSGrid = new THREE.SGridWireGeometry(nx, ny, nz, nodes, 0xffffff);
	//var wireSGrid = new THREE.Mesh(cubeGeometry, new THREE.MeshLambertMaterial({color:0xffffff, side: THREE.DoubleSide, wireframe: true}))
	wireSGrid.name = 'wireframe';
	sGrid.add(wireSGrid);

	//var edges = CreateModelBound(sGrid, 0x00ff00);
	edges = new THREE.EdgesGeometry( sGrid, 0x00ff00 );
	edges.name = "edge";
	sGrid.add(edges);

	sGrid.userData = {'Model':true, 'edge':true, 'wireframe':true};
	scene.add(sGrid);
	render();
	
	func(sGrid);
}

function AddGObj(gobjData, func)
{
	var name = gobjData.TSurfData.Name;
	var color = gobjData.TSurfData.Color;
	var node = gobjData.VRTX;
	var face = gobjData.TRGL;

	var geom = new THREE.Geometry();
	for(var i = 0 ; i < node.length ; i++)
	{
		geom.vertices.push(new THREE.Vector3(node[i][0],node[i][1],node[i][2]));
	}

	for(var i = 0 ; i < face.length ; i++)
	{
		geom.faces.push( new THREE.Face3( face[i][0], face[i][1], face[i][2] ) );
	}

	geom.mergeVertices();

	geom.computeBoundingBox();
	geom.verticesNeedUpdate = true;
    geom.normalsNeedUpdate = true;
	geom.computeBoundingSphere();
    geom.computeFaceNormals();
    geom.computeVertexNormals ();

    var newColor = RGBArrayToHex(color);

    var material = new THREE.MeshLambertMaterial( {color: newColor, side:THREE.DoubleSide, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0} );
	var surface = new THREE.Mesh( geom, material );
	surface.name = name;

	var wirematerial = new THREE.MeshBasicMaterial( {color: 0xffffff, wireframe: true} );
	var wireSurface= new THREE.Mesh(geom, wirematerial);
	wireSurface.name = "wireframe";

	surface.add(wireSurface);

	edges = new THREE.EdgesGeometry( surface, 0x00ff00, 180 );
	edges.name = "edge";
	surface.add(edges);

	surface.userData = {'Model':true, 'edge':true, 'wireframe':true};

	scene.add( surface );
	render();

	func(surface);
}

function AddWell(wellData, func)
{
	var x = wellData.x;
	var y = wellData.y;
	var z = wellData.z;
	var dir = wellData.dir;
	var angle = wellData.angle;
	var name = wellData.holeno;

	var height = 100;

	var geometry = new THREE.CylinderGeometry( 2, 2, height, 12 );
	var material = new THREE.MeshLambertMaterial( {color: 0xffff00, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0} );
	var cylinder = new THREE.Mesh( geometry, material );
	cylinder.geometry.computeBoundingBox();
	cylinder.name = name;

	var wirematerial = new THREE.MeshBasicMaterial( {color: 0xffffff, wireframe: true} );
	var wireCylider= new THREE.Mesh(geometry, wirematerial);

	wireCylider.name = 'wireframe';
	cylinder.add(wireCylider);

	cylinder.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(0, -(height/2), 0) );
	cylinder.geometry.applyMatrix( new THREE.Matrix4().makeRotationX(Math.PI/180 * angle *-1) );
	cylinder.geometry.applyMatrix( new THREE.Matrix4().makeRotationY(Math.PI/180 * dir * -1) );
	cylinder.geometry.applyMatrix( new THREE.Matrix4().makeTranslation(x, y, z) );

	edges = new THREE.EdgesGeometry( cylinder, 0x00ff00 );
	edges.name = "edge";
	cylinder.add(edges);

	cylinder.userData = {'Model':true, 'edge':true, 'wireframe':true};
	scene.add( cylinder );
	render();

	func(cylinder);
}

function SetGridRegion(modelID, fileName, cellSize)
{
	var oReq = new XMLHttpRequest();
	oReq.open("GET", fileName, true);
	oReq.responseType = "text";

	oReq.onload = function (oEvent) {
		var arrayBuffer = oReq.responseText;
		var regionIndex = JSON.parse(arrayBuffer);
		var model = scene.getObjectById(modelID);
		CreateRegion(model, regionIndex);
	};

	oReq.send(null);
}

function FitAll()
{
	var aabbMin = new THREE.Vector3(Number.MAX_VALUE, Number.MAX_VALUE, Number.MAX_VALUE);
    var aabbMax = new THREE.Vector3(-Number.MAX_VALUE, -Number.MAX_VALUE, -Number.MAX_VALUE);
    var radius = 0;
    
    var sceneChild = scene.children;
    
    for( var i = sceneChild.length-1 ; i >= 0 ; i--)
    {
    	if (sceneChild[i].visible == false)
    		continue;
    	
    	if(sceneChild[i].type == "Mesh")
    	{
            var geo = sceneChild[i];
            ModelBoundary(aabbMin, aabbMax, geo.geometry.boundingBox);
    	}
    	else if(sceneChild[i].type == "Object3D")
    	{
    		for( var j = sceneChild[i].children.length-1 ; j >= 0 ; j--)
    		{
    			var geo = sceneChild[i].children[j];
    			ModelBoundary(aabbMin, aabbMax, geo.geometry.boundingBox);
    		}
    	}
    }
    
    var aabbCenter = new THREE.Vector3();
    aabbCenter.x = (aabbMax.x + aabbMin.x) * 0.5;
    aabbCenter.y = (aabbMax.y + aabbMin.y) * 0.5;
    aabbCenter.z = (aabbMax.z + aabbMin.z) * 0.5;
    
    var diag = new THREE.Vector3();
    diag = diag.subVectors(aabbMax, aabbMin);
    var radius = diag.length() * 0.5;
    
    
    var offset = radius / Math.tan(Math.PI / 180.0 * camera.fov * 0.5);
    var dir = new THREE.Vector3();
    //dir.set(camera.matrix.elements[8], camera.matrix.elements[9], camera.matrix.elements[10]);
    dir.set(0, 0, 1);
    
    dir.multiplyScalar(offset); 
    var newPos = new THREE.Vector3();
    newPos.addVectors(aabbCenter, dir);
    camera.rotationAutoUpdate = false;
    camera.position.set( newPos.x, newPos.y, newPos.z );
    camera.lookAt(aabbCenter);  
    
    camera.near = 1;
    camera.far = radius*10;
    
    camera.rotationAutoUpdate = true;
    camera.updateProjectionMatrix(); 

    ResetControls(aabbCenter);
    controls.update();
    render();
}

function ClearModelMemory(model)
{
	childs = model.children;

	for(var i = model.children.length-1 ; i >=0 ; i--)
	{
		ClearModelMemory(model.children[i]);
	}

	if(model.type == "Mesh" || model.type == "Line")
	{

		model.geometry.vertices = null;
		model.geometry.faces = null;
		model.geometry.dispose();
		model.material.dispose();
	}

}

function InitVoxet(modelID)
{
	var model = scene.getObjectById(modelID);
	
	if(model === undefined)
		return;
	
	var newmaterial = new THREE.MeshLambertMaterial( { color: 0xeeeeee, side: THREE.DoubleSide, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0});

	model.geometry.colorsNeedUpdate = true
	model.material = newmaterial;
	
	render();
}

function InitSGrid(modelID)
{
	var model = scene.getObjectById(modelID);
	
	if(model === undefined)
		return;
	
	var newmaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, side: THREE.DoubleSide, opacity: 1, transparent: true, wireframe: true});
	model.geometry.colorsNeedUpdate = true
	model.material = newmaterial;
	render();
}

function ChangeWireframeColor(model, rgbColor)
{
	var child = model.children;
	for(var i = child.length-1 ; i >= 0 ; i--)
	{
		ChangeWireframeColor(child[i], rgbColor);
	}

	if(model.name == "wireframe")
	{
		var oldMaterialSide = model.material.side;

		var newColor = RGBArrayToHex(rgbColor);
		var newMaterial = new THREE.MeshBasicMaterial({color:newColor, side:oldMaterialSide, wireframe:true});
		model.material = newMaterial;
	}
}

function ChangeEdgeColor(model, rgbColor)
{
	var child = model.children;
	for(var i = child.length-1 ; i >= 0 ; i--)
	{
		ChangeEdgeColor(child[i], rgbColor);
	}

	if(model.name == "edge")
	{
		var oldMaterialSide = model.material.side;
		var oldMaterialWire = model.material.wireframe;

		var newColor = RGBArrayToHex(rgbColor);
		var newMaterial = new THREE.MeshBasicMaterial( { color: newColor } );
		model.material = newMaterial;
	}
}

function ChangeRandomColor(model)
{
	var child = model.children;

	for(var i = child.length-1 ; i >= 0 ; i--)
	{
		if(child[i].name == "Universe")
			continue;
		else
		{
			var newColor = 	"#"+((1<<24)*Math.random()|0).toString(16);
			var subChild = child[i].children;
			for(var j = subChild.length-1 ; j>=0 ; j--)
			{
				if(subChild[j].name == "edge")
					continue;
				if(subChild[j].name == "wireframe")
					continue;

				var oldMaterialSide = subChild[j].material.side;
				var oldMaterialWire = subChild[j].material.wireframe;

				var newMaterial = new THREE.MeshLambertMaterial( { color: newColor, side: oldMaterialSide, wireframe: oldMaterialWire, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0 } );
				subChild[j].material = newMaterial;
			}
		}
	}

	for(var i = child.length-1 ; i >= 0 ; i--)
	{
		ChangeRandomColor(child[i]);
	}
}

function ChangeColor(model, rgbColor)
{
	var child = model.children;
	for(var i = child.length-1 ; i >= 0 ; i--)
	{
		ChangeColor(child[i], rgbColor);
	}

	if(model.type == "Object3D")
		return;
	if(model.name == "edge")
		return;
	if(model.name == "wireframe")
		return;

	var newColor;
	if(typeof(rgbColor) === 'string')
	{
		rgbColor = rgbColor.replace("#", "0x");
		newColor = parseInt(rgbColor, 16);
	}
	else
		newColor = RGBArrayToHex(rgbColor);
	
	var oldMaterialSide = model.material.side;
	var oldMaterialWire = model.material.wireframe;

	var newMaterial = new THREE.MeshLambertMaterial( { color: newColor, side: oldMaterialSide, wireframe: oldMaterialWire, polygonOffset:true, polygonOffsetFactor:1.0, polygonOffsetUnits:4.0 } );

	model.material = newMaterial;
}

function RemoveTempObject()
{
	for ( i = scene.children.length - 1; i >= 0 ; i -- ) 
	{
		if(scene.children[i].name == "temp_boundary" || scene.children[i].name == "temp_section")
			scene.remove(scene.children[ i ]);
	}
}

function GetWireframeVisible(model, modelType)
{
	var child = model.children;

	if(modelType == "Model3D")
		return child[0].userData.wireframe;
	else
		return model.userData.wireframe;
}

function GetEdgeVisible(model, modelType)
{
	var child = model.children;

	if(modelType == "Model3D")
		return child[0].userData.edge;
	else
		return model.userData.edge;
}

function GetObjectColor(model, modelType)
{
	if(modelType == "Model3D")
	{
		var child = model.children;
		return child[0].material.color;
	}
	else
	{
		return model.material.color;
	}
}
function GetWireframeColor(model, modelType)
{
	var child = model.children;
	if(modelType == "Model3D")
	{
		var subChild = child[0].children;

		for(var j = 0 ; j < subChild.length ; j++)
		{
			if(subChild[j].name == "wireframe")
				return subChild[j].material.color;
		}

		return THREE.color(0, 0, 0);
	}
	else
	{
		return child[0].material.color;
	}
}

function GetEdgeColor(model, modelType)
{
	var child = model.children;
	if(modelType == "Model3D")
	{
		var subChild = child[0].children;

		for(var j = 0 ; j < subChild.length ; j++)
		{
			if(subChild[j].name == "edge")
				return subChild[j].material.color;
		}

		return THREE.color(0, 0, 0);
	}
	else
	{
		return child[1].material.color;
	}
}

function ChangeWireframeVisible(modelType, modelID, visible)
{
	var model = scene.getObjectById(modelID);
	if(model === undefined)
		return;

	var child = model.children;
	if(modelType == "Model3D")
	{
		for(var i = 0 ; i < child.length ; i++)
		{
			var subChild = child[i].children;

			for(var j = 0 ; j < subChild.length ; j++)
			{
				if(subChild[j].name == "wireframe")
					subChild[j].visible = visible;
			}
			child[i].userData.wireframe = visible;
		}
	}
	else
	{
		for(var i = 0 ; i < child.length ; i++)
		{
			if(child[i].name == "wireframe")
				child[i].visible = visible;
		}
		model.userData.wireframe = visible;
	}

	render();
}

function ChangeEdgeVisible(modelType, modelID, visible)
{
	var model = scene.getObjectById(modelID);
	if(model === undefined)
		return;

	var child = model.children;
	if(modelType == "Model3D")
	{
		for(var i = 0 ; i < child.length ; i++)
		{
			var subChild = child[i].children;

			for(var j = 0 ; j < subChild.length ; j++)
			{
				if(subChild[j].name == "edge")
					subChild[j].visible = visible;
			}
			child[i].userData.edge = visible;
		}
	}
	else
	{
		for(var i = 0 ; i < child.length ; i++)
		{
			if(child[i].name == "edge")
				child[i].visible = visible;
		}
		model.userData.edge = visible;
	}

	render();
}

function DeleteSGridSection(modelID)
{
	var model = scene.getObjectById(modelID);
	
	if(model === undefined)
		return;

	if(model.userData.Model)
		ObjectVisibleChange(modelID, true);
	else
		ObjectVisibleChange(modelID, false);

	RemoveTempObject();
	
	render();
}

function ShowSGridSection(modelID, axis, offset)
{
	var model = scene.getObjectById(modelID);
	
	if(model === undefined)
		return;
	
	model.visible = false;
	
	var boundary = CreateModelBound(model, 0x00ff00);
	boundary.name = "temp_boundary";
	scene.add( boundary );
	
	
	var section = CreateSectionCube(model, axis, offset);
	var xSeg = model.geometry.widthSegments;
	var ySeg = model.geometry.heightSegments;
	var zSeg = model.geometry.depthSegments;
	
	var attribute = [];
	if(axis == 'X')
	{
		for(var z = 0 ; z < model.geometry.depthSegments ; z++)
		{
			var index = z*xSeg*ySeg;
			for(var y = 0 ; y < model.geometry.heightSegments ; y++)
			{
				var temp = index + xSeg*y + offset;
				attribute.push(propertyList[temp]);
			}
		}
	}
	else if(axis == 'Y')
	{
		for(var z = 0 ; z < zSeg ; z++)
		{
			var index = z*xSeg*ySeg;
			for(var x = 0 ; x < xSeg ; x++)
			{
				var temp = index + x + offset*xSeg;
				attribute.push(propertyList[temp]);
			}
		}
	}
	else
	{
		for(var y = 0 ; y < ySeg ; y++)
		{
			var index = offset*xSeg*ySeg + y*xSeg;
			for(var x = 0 ; x < xSeg ; x++)
			{
				var temp = index + x;
				attribute.push(propertyList[temp]);
			}
		}
	}
	
	//SetGeometryFaceColor(section, attribute);
	var newmaterial = new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors });
	var sectionModel = new THREE.Mesh(section, newmaterial);
	

	SetGridFaceColor(sectionModel, attribute);
	sectionModel.name = "temp_section";
	scene.add(sectionModel);
	
	render();
	
	if(axis == 'X')
	{
		return model.geometry.widthSegments-1;
	}
	else if(axis == 'Y')
	{
		return model.geometry.heightSegments-1;
	}
	else if(axis == 'Z')
	{
		return model.geometry.depthSegments-1;
	}
}

function ObjectVisibleChange(modelID, checked)
{
	var model = scene.getObjectById(modelID);
	if(model === undefined)
		return;

	if(model.type == "Object3D")
	{
		model.visible  = checked;
		var child = model.children;
		for(var i = child.length-1 ; i >= 0 ; i--)
		{
			child[i].userData.Model = checked;
		}
		if(checked && model.userData.edge == false)
		{
			var child = model.children;
			for(var i = child.length-1 ; i >= 0 ; i--)
			{
				var subchild = child[i].children;
				for(var j = subchild.length-1 ; j >= 0 ; j--)
				{
					if(subchild[j].name == "edge")
						subchild[j].visible = false;
				}

				if(child[i].name == "edge")
					child[i].visible = false;
			}
		}

		if(checked && model.userData.wireframe == false)
		{
			var child = model.children;
			for(var i = child.length-1 ; i >= 0 ; i--)
			{
				var subchild = child[i].children;
				for(var j = subchild.length-1 ; j >= 0 ; j--)
				{
					if(subchild[j].name == "wireframe")
						subchild[j].visible = false;
				}

				if(child[i].name == "wireframe")
					child[i].visible = false;
			}
		}
	}
	else
	{
		model.visible = checked;
		model.userData.Model = checked;
		if(checked && model.userData.edge == false)
		{
			var child = model.children;
			for(var i = child.length-1 ; i >= 0 ; i--)
			{
				var subchild = child[i].children;
				for(var j = subchild.length-1 ; j >= 0 ; j--)
				{
					if(subchild[j].name == "edge")
						subchild[j].visible = false;
				}

				if(child[i].name == "edge")
					child[i].visible = false;
			}
		}

		if(checked && model.userData.wireframe == false)
		{
			var child = model.children;
			for(var i = child.length-1 ; i >= 0 ; i--)
			{
				var subchild = child[i].children;
				for(var j = subchild.length-1 ; j >= 0 ; j--)
				{
					if(subchild[j].name == "wireframe")
						subchild[j].visible = false;
				}

				if(child[i].name == "wireframe")
					child[i].visible = false;
			}
		}
	}

	render();
}

init();
animate();