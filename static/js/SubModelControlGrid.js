var SubPropGridObj = $('#propertyGrid');
var SubPropGridName = "";
var SubPropGridID = -1;
function ClearDataGrid()
{
	SubPropGridName = "";
	SubPropGridID = -1;
	SubPropGridObj.datagrid('loadData', {"total":0,"rows":[]});
}

function GetSubPropGridName()
{
	return [SubPropGridName, SubPropGridID];
}

function InitializeModel3DDataGrid(node)
{
	SubPropGridName = "Model3D";
	SubPropGridID = node.id;
    var regionInfo = node.attributes;
	SubPropGridObj.datagrid({singleSelect:true, selectOnCheck:false, checkOnSelect:false, showHeader:false});
	SubPropGridObj.datagrid({
		columns:[[
			{field:'id',title:'Item ID',hidden:true},
			{field:'visible',title:'Visible',checkbox:true},
			{field:'name',title:'Name',width:170},
			{field:'color', title:'Color', width:90, editor:'colorpicker'},
		]]
	});

	for(var i = 0 ; i < regionInfo.length ; i++)
	{
		var model = scene.getObjectById(regionInfo[i].id);
		var visible1 = model.visible;
		var name = regionInfo[i].text;

		var rgbColor = GetObjectColor(model, "Model3D");
		var objectColor = RGBObjectToHex( rgbColor);
		var hexColor = "#" + objectColor;

		SubPropGridObj.datagrid('appendRow', {"id": regionInfo[i].id, "checked":visible1, "name":name, 'color':hexColor});
		var col = SubPropGridObj.datagrid('getColumnOption','color');
		hexColor = colourNameToHex(hexColor);
		col.styler = function(){return 'background-color:'+hexColor;};
		SubPropGridObj.datalist('refreshRow',i);
	}
}

function InitializeVoxetDataGrid(node)
{
	SubPropGridName = "Voxet";
	SubPropGridID = node.id;

	var propertyInfo = node.attributes;
	SubPropGridObj.datagrid({singleSelect:true, checkOnSelect:true, selectOnCheck:true, showHeader:true});
	SubPropGridObj.datagrid({
		columns:[[
			{field:'id',title:'Item ID',hidden:true},
			{field:'visible',title:'Visible',checkbox:true},
			{field:'name',title:'Name',width:150},
			{field:'dataType',title:'DataType',width:40},
			{field:'propertyKind',title:'PropertyKind',width:40},
			{field:'u',title:'U',width:30},
			{field:'v',title:'V',width:30},
			{field:'w',title:'W',width:30},
		]]
	});

	SubPropGridObj.datagrid('appendRow', {"id": -1, "checked":false, "name":'None', "dataType":'none', "propertyKind":'none', "u":0,"v":0,"w":0});
	for(var i = propertyInfo.length - 1 ; i >= 0 ; i--)
	{
		var propertyID = propertyInfo[i].ObjID;
		var name = propertyInfo[i].text;
		var dataType = propertyInfo[i].DataType;
		var propertyKind = propertyInfo[i].PropertyKind;
		var u = propertyInfo[i].U;
		var v = propertyInfo[i].V;
		var w = propertyInfo[i].W;
		SubPropGridObj.datagrid('appendRow', {"id": propertyID, "checked":false, "name":name, "dataType":dataType, "propertyKind":propertyKind, "u":u,"v":v,"w":w});
	}
	SubPropGridObj.datagrid('checkRow', 0);
}

function SetCheckDataGrid(checked)
{
    var rows = SubPropGridObj.datagrid('getRows')
	for(var i = rows.length - 1 ; i >= 0 ; i--)
	{
		if(checked == true)
			SubPropGridObj.datagrid('checkRow',i);
		else
			SubPropGridObj.datagrid('uncheckRow',i);
	}
}