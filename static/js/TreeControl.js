var treeObj = $('#objectTree');
var oldNode = -1;
var oldRoot = "";
$(function(){
	$.extend($.fn.tree.methods, {
	    getLevel:function(jq,target){ 
	        var l = $(target).parentsUntil("ul.tree","ul"); 
	        return l.length+1; 
	    } 
	}); 	
});

function SetTreeNode(nodeID, category)
{
	oldNode = nodeID;
	oldRoot = category;
}

function GetTreeNode()
{
	return oldNode;
}
function GetOldCategory()
{
	return oldRoot;
}

function GetRootTreeNode(node)
{
	var lv =  $('#objectTree').tree("getLevel",node.target);
	
	if( lv == 0)
	{
		return null;
	}
	else
	{
		var root = node;
		
		for( var i = 1 ; i < lv ; i++)
		{
			root = $('#objectTree').tree("getParent", root.target);
		}
		
		return root;
	}		
}

function AddModelToTree(modelData)
{
	var node = treeObj.tree('getRoots');
	
	var child = [];
	var children = modelData.children;
	for(var i= children.length-1 ; i >= 0 ; i--)
	{
		var temp = {"text":children[i].name, "id":children[i].id}
		child.push(temp);
	}
	
	var obj =[{"id":modelData.id, "text":modelData.name, "checked":true, "attributes":child}];
	
	treeObj.tree('append', {
		parent:node[0].target,
		data:obj
	});			
}
function addGObjToTree(model)
{
	var node = treeObj.tree('getRoots');

	var obj =[{"id":model.id, "checked":true, "text":model.name}];
	treeObj.tree('append', {
		parent:node[4].target,
		data:obj
	});
}
function addWellToTree(model)
{
	var node = treeObj.tree('getRoots');

	var obj =[{"id":model.id, "checked":true, "text":model.name}];
	treeObj.tree('append', {
		parent:node[3].target,
		data:obj
	});
}
function deepCopy(obj) {
    if (Object.prototype.toString.call(obj) === '[object Array]') {
        var out = [], i = 0, len = obj.length;
        for ( ; i < len; i++ ) {
            out[i] = arguments.callee(obj[i]);
        }
        return out;
    }
    if (typeof obj === 'object') {
        var out = {}, i;
        for ( i in obj ) {
            out[i] = arguments.callee(obj[i]);
        }
        return out;
    }
    return obj;
}

function addSGridToTree(model, property)
{
	var node = treeObj.tree('getRoots');

	var propertyCopy = [];
	for(var i = 0 ; i < property.length ; i++)
		propertyCopy.push(property[i]);
	
	var obj =[{"id":model.id, "checked":true, "text":model.name, "attributes":propertyCopy}];
	treeObj.tree('append', {
		parent:node[2].target,
		data:obj
	});	
}
function addVoxetImageToTree(model)
{
	var node = treeObj.tree('getRoots');

	var obj =[{"id":model.id, "text":model.name, "checked":true, "attributes":{"DataType":"RGBA"}}];
	treeObj.tree('append', {
		parent:node[1].target,
		data:obj
	});
}

function addVoxetToTree(model, properties, regions)
{
	var node = treeObj.tree('getRoots');
	
	var pchild = [];
	for(var i = properties.length-1 ; i >= 0 ; i--)
	{
		var temp = {"text":properties[i].propertyname, "ObjID":properties[i].gobjid, "DataType":properties[i].datatype, "PropertyKind":properties[i].propertykind, "U":model.geometry.widthSegments, "V":model.geometry.heightSegments, "W":model.geometry.depthSegments};
		pchild.push(temp);
	}

	var obj =[{"id":model.id, "text":model.name, "checked":true, "attributes":pchild}];
	treeObj.tree('append', {
		parent:node[1].target,
		data:obj
	});
}