function Jsonparse(evt, func)
{
	var files = evt.target.files; // FileList object

	if(files.length == 0)
		return;
			
	var file = files[0];
			
	var loaded = 0;
	var total = file.size;
	var step = 1024*1024;
	var start = 0;
			
	var jsonData = "";
	var reader = new FileReader();
	reader.onload = function (oFREvent) {
		loaded += step;
	    jsonData += oFREvent.target.result ; 
	              
	    if(loaded <= total)
	    {
	    	blob = file.slice(loaded, loaded+step);
	        reader.readAsText(blob);
	    }
	    else
	    {
	    	var json = JSON.parse(jsonData);
	    	func(json);
	    }
    };
            
    var blob = file.slice(start, step);
    reader.readAsText(blob);
}