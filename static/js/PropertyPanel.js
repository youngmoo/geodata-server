function HidePropertyPanel(parentPanel)
{
	var children = parentPanel.childNodes;
	for(var i = children.length-1 ; i >= 0 ; i--)
	{
		if (children[i].nodeName != "DIV")
			continue;
		children[i].style.display = 'none'
	}
}

function ShowPropertyPanel(parentPanel, targetPanel)
{
	var children = parentPanel.childNodes;
	for(var i = children.length-1 ; i >= 0 ; i--)
	{
		if (children[i].nodeName != "DIV")
			continue;
		if(children[i].id == targetPanel)
		{
			children[i].style.display = 'block';
		}
		else
		{
			children[i].style.display = 'none'
		}
	}
}