import math
import numpy as np

class SurveyLine:
    start_ = []
    end_ = []
    avgElev_  = 0
    unitvec_ = []
    v_dirvec_ = []

    def __init__(self, startx, starty, endx, endy, elev):
        self.start_ = [startx, starty]
        self.end_ = [endx,endy ]
        self.avgElev_ = elev
        self.unitvec_ = [endx-startx, endy-starty]

        norm = np.linalg.norm(self.unitvec_)
        temp = self.unitvec_/norm
        unit3d = [temp[0], temp[1], 0]

        self.v_dirvec_ = np.cross([0,0, 1], unit3d)

    def V_unitVec(self):
        return self.v_dirvec_

    def LineLength(self, pt1, pt2):
        return np.linalg.norm(np.subtract(pt2,pt1))