# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0014_logvaluedouble'),
    ]

    operations = [
        migrations.CreateModel(
            name='Logvaluetext',
            fields=[
                ('welllogvalueid2', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'wellLogValueId', serialize=False, to='polls.Welllogvalue')),
                ('depth', models.FloatField()),
                ('text', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'LogValueText',
                'managed': False,
            },
            bases=('polls.welllogvalue',),
        ),
    ]
