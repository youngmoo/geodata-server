# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0003_ymlee_base_ymlee_child'),
    ]

    operations = [
        migrations.CreateModel(
            name='Model3D',
            fields=[
                ('gobjid', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'gobjId', serialize=False, to='polls.Odbbase')),
                ('objectname', models.TextField(null=True, db_column=b'objectName', blank=True)),
                ('objectdata', models.TextField(null=True, db_column=b'objectData', blank=True)),
                ('json', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'model3d',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
    ]
