# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0009_odbvoxetregion'),
    ]

    operations = [
        migrations.CreateModel(
            name='Well',
            fields=[
                ('gobjid', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'gobjId', serialize=False, to='polls.Odbbase')),
                ('holeno', models.TextField(null=True, blank=True)),
                ('address', models.TextField(null=True, blank=True)),
                ('x', models.FloatField(null=True, blank=True)),
                ('y', models.FloatField(null=True, blank=True)),
                ('z', models.FloatField(null=True, blank=True)),
                ('dir', models.IntegerField()),
                ('angle', models.IntegerField()),
            ],
            options={
                'db_table': 'well',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
    ]
