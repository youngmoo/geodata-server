# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0004_model3d'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gobj',
            fields=[
                ('gobjid', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'gobjId', serialize=False, to='polls.Odbbase')),
                ('objecttype', models.TextField(null=True, db_column=b'objectType', blank=True)),
                ('objectname', models.TextField(null=True, db_column=b'objectName', blank=True)),
                ('feature', models.TextField(null=True, blank=True)),
                ('objectdata', models.TextField(null=True, db_column=b'objectData', blank=True)),
            ],
            options={
                'db_table': 'gobj',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
        migrations.RemoveField(
            model_name='ymlee_child',
            name='ymlee_base_ptr',
        ),
        migrations.DeleteModel(
            name='ymlee_base',
        ),
        migrations.DeleteModel(
            name='ymlee_child',
        ),
    ]
