# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_odbbase_project'),
    ]

    operations = [
        migrations.CreateModel(
            name='ymlee_base',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='ymlee_child',
            fields=[
                ('ymlee_base_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='polls.ymlee_base')),
                ('code', models.IntegerField(default=0)),
            ],
            bases=('polls.ymlee_base',),
        ),
    ]
