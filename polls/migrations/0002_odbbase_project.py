# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Odbbase',
            fields=[
                ('comments', models.TextField(null=True, blank=True)),
                ('user', models.TextField(null=True, blank=True)),
                ('version', models.IntegerField()),
                ('datemodified', models.TextField(null=True, db_column=b'dateModified', blank=True)),
                ('gobjid2', models.IntegerField(serialize=False, primary_key=True, db_column=b'gobjId')),
                ('typeid', models.TextField()),
            ],
            options={
                'db_table': 'OdbBase',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('odbbase_ptr', models.OneToOneField(parent_link=True, auto_created=True, to='polls.Odbbase')),
                ('gobjid', models.IntegerField(serialize=False, primary_key=True, db_column=b'gobjId')),
                ('projectname', models.TextField(null=True, db_column=b'projectName', blank=True)),
            ],
            options={
                'db_table': 'project',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
    ]
