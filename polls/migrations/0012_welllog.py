# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0011_wellset'),
    ]

    operations = [
        migrations.CreateModel(
            name='Welllog',
            fields=[
                ('welllogid', models.IntegerField(serialize=False, primary_key=True)),
                ('logname', models.TextField(db_column=b'logName')),
                ('datatype', models.TextField()),
                ('user', models.TextField()),
            ],
            options={
                'db_table': 'WellLog',
                'managed': False,
            },
        ),
    ]
