# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0012_welllog'),
    ]

    operations = [
        migrations.CreateModel(
            name='Welllogvalue',
            fields=[
                ('welllogvalueid', models.AutoField(serialize=False, primary_key=True, db_column=b'wellLogValueId')),
                ('typeid', models.TextField()),
            ],
            options={
                'db_table': 'WellLogValue',
                'managed': False,
            },
        ),
    ]
