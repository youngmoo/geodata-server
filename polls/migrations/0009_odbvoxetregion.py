# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0008_odbvoxetproperty'),
    ]

    operations = [
        migrations.CreateModel(
            name='Odbvoxetregion',
            fields=[
                ('gobjid', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'gobjId', serialize=False, to='polls.Odbbase')),
                ('regionname', models.TextField(null=True, blank=True)),
                ('blob', models.BinaryField(null=True, blank=True)),
            ],
            options={
                'db_table': 'OdbVoxetRegion',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
    ]
