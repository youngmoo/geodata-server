# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0010_well'),
    ]

    operations = [
        migrations.CreateModel(
            name='Wellset',
            fields=[
                ('wellsetid', models.IntegerField(serialize=False, primary_key=True)),
                ('setname', models.TextField(null=True, blank=True)),
                ('interpolation', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'WellSet',
                'managed': False,
            },
        ),
    ]
