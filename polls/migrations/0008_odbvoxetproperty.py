# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0007_voxet'),
    ]

    operations = [
        migrations.CreateModel(
            name='Odbvoxetproperty',
            fields=[
                ('gobjid', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'gobjId', serialize=False, to='polls.Odbbase')),
                ('propertyname', models.TextField(null=True, blank=True)),
                ('propertykind', models.TextField(null=True, blank=True)),
                ('family', models.TextField(null=True, blank=True)),
                ('propertyclass', models.TextField(null=True, blank=True)),
                ('propertyclasstype', models.TextField(null=True, db_column=b'propertyClassType', blank=True)),
                ('blob', models.BinaryField(null=True, blank=True)),
                ('interpolation', models.TextField(null=True, blank=True)),
                ('elementcount', models.IntegerField()),
                ('classification', models.TextField(null=True, blank=True)),
                ('datatype', models.TextField(null=True, blank=True)),
                ('ndv', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'OdbVoxetProperty',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
    ]
