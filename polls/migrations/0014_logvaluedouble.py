# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0013_welllogvalue'),
    ]

    operations = [
        migrations.CreateModel(
            name='Logvaluedouble',
            fields=[
                ('welllogvalueid2', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'wellLogValueId', serialize=False, to='polls.Welllogvalue')),
                ('depth', models.FloatField()),
                ('value', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'LogValueDouble',
                'managed': False,
            },
            bases=('polls.welllogvalue',),
        ),
    ]
