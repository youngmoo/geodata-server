# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0005_auto_20150803_0959'),
    ]

    operations = [
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('gobjid', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'gobjId', serialize=False, to='polls.Odbbase')),
                ('surveytype', models.TextField(null=True, db_column=b'surveyType', blank=True)),
                ('surveyname', models.TextField(null=True, db_column=b'surveyName', blank=True)),
                ('blob', models.BinaryField(null=True, blank=True)),
                ('startx', models.FloatField(null=True, blank=True)),
                ('starty', models.FloatField(null=True, blank=True)),
                ('startz', models.FloatField(null=True, blank=True)),
                ('endx', models.FloatField(null=True, blank=True)),
                ('endy', models.FloatField(null=True, blank=True)),
                ('endz', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'survey',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
    ]
