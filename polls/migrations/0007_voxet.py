# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0006_survey'),
    ]

    operations = [
        migrations.CreateModel(
            name='Voxet',
            fields=[
                ('gobjid', models.OneToOneField(parent_link=True, primary_key=True, db_column=b'gobjId', serialize=False, to='polls.Odbbase')),
                ('nu', models.IntegerField()),
                ('nv', models.IntegerField()),
                ('nw', models.IntegerField()),
                ('originx', models.FloatField(null=True, blank=True)),
                ('originy', models.FloatField(null=True, blank=True)),
                ('originz', models.FloatField(null=True, blank=True)),
                ('stepux', models.FloatField(null=True, blank=True)),
                ('stepuy', models.FloatField(null=True, blank=True)),
                ('stepuz', models.FloatField(null=True, blank=True)),
                ('stepvx', models.FloatField(null=True, blank=True)),
                ('stepvy', models.FloatField(null=True, blank=True)),
                ('stepvz', models.FloatField(null=True, blank=True)),
                ('stepwx', models.FloatField(null=True, blank=True)),
                ('stepwy', models.FloatField(null=True, blank=True)),
                ('stepwz', models.FloatField(null=True, blank=True)),
                ('voxetname', models.TextField(null=True, blank=True)),
                ('modelingrole', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'voxet',
                'managed': False,
            },
            bases=('polls.odbbase',),
        ),
    ]
