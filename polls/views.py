from django.shortcuts import get_object_or_404, render, render_to_response
from django.template.context import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import  reverse
from models import Project, Odbbase, Model3D, Gobj, Survey, Voxet, Odbvoxetproperty, Odbvoxetregion, Well, Wellset, Welllog, Welllogvalue, Logvaluedouble, Logvaluetext
from SurveyLine import SurveyLine
from SNTRReader import SNTRRead
from django.core.exceptions import ObjectDoesNotExist
import json
import base64
import struct
import socket
from PIL import Image
from datetime import datetime

# Create your views here.
class MultipleChildObjectsReturned(Exception):
    def __init__(self, len_child_objects):
        self.len_child_objects = len_child_objects
    def __str__(self):
        return repr(self.len_child_objects)

def get_child_object(parental_object):
    child_objects = []

    all_field_names = parental_object._meta.get_all_field_names()

    for field_name in all_field_names:
        field = None
        child_object = None

        try:
            field = parental_object.__getattribute__(field_name)
        except ObjectDoesNotExist:
            continue

        if issubclass(field.__class__, parental_object.__class__):
            child_object = field
        else:
            continue

        if field_name == child_object.__class__.__name__.lower():
            child_objects.append(child_object)
        else:
            continue

    if len(child_objects) == 1:
        return child_objects[0]
    else:
        raise MultipleChildObjectsReturned(len(child_objects))

def ValuesQuerySetToDict(vqs):
    return [item for item in vqs]

def MakeVoxetHeader(voxetObject, propertyType, ImageData):
    jsonData = {}
    objName = getattr(voxetObject, 'voxetname')
    jsonData['Name'] = objName
    jsonData['Type'] = 'Image'
    jsonData['Origin'] = [getattr(voxetObject, 'originx'), getattr(voxetObject, 'originy'), getattr(voxetObject, 'originz')]
    nu = getattr(voxetObject, 'nu')
    nv = getattr(voxetObject, 'nv')
    nw = getattr(voxetObject, 'nw')
    jsonData['U'] = [getattr(voxetObject, 'stepux'), getattr(voxetObject, 'stepuy'), getattr(voxetObject, 'stepuz')]
    jsonData['V'] = [getattr(voxetObject, 'stepvx'), getattr(voxetObject, 'stepvy'), getattr(voxetObject, 'stepvz')]
    jsonData['W'] = [getattr(voxetObject, 'stepwx'), getattr(voxetObject, 'stepwy'), getattr(voxetObject, 'stepwz')]
    jsonData['DIR_NUM'] = [nu, nv, nw]

    if(propertyType == "RGBA"):
        img = Image.frombytes("RGBA", (nu, nv), ImageData[4:])
    else:
        img = Image.frombytes("P", (nu, nv), ImageData[4:])

    currDate = datetime.now().strftime('%m_%d_%H_%M_%S')
    imageName = "%s_%s.png" % (objName, currDate)
    img.save(".\\GeoSite\\image\\%s" % imageName)

    result = {'Voxetdata': jsonData, "ImageFile":imageName}

    return result

def MakeVoxetJson(voxetObject, propertyData, regionData):
    jsonData = {}
    objName = getattr(voxetObject, 'voxetname')
    jsonData['Name'] = objName
    jsonData['Type'] = 'Model'
    jsonData['Origin'] = [getattr(voxetObject, 'originx'), getattr(voxetObject, 'originy'), getattr(voxetObject, 'originz')]
    nu = getattr(voxetObject, 'nu')
    nv = getattr(voxetObject, 'nv')
    nw = getattr(voxetObject, 'nw')
    if(nu == 1):
        jsonData['U'] = [getattr(voxetObject, 'stepux'), getattr(voxetObject, 'stepuy'), getattr(voxetObject, 'stepuz')]
    else:
        jsonData['U'] = [x/(nu-1) for x in [getattr(voxetObject, 'stepux'), getattr(voxetObject, 'stepuy'), getattr(voxetObject, 'stepuz')]]

    if(nv == 1):
        jsonData['V'] = [getattr(voxetObject, 'stepvx'), getattr(voxetObject, 'stepvy'), getattr(voxetObject, 'stepvz')]
    else:
        jsonData['V'] = [x/(nv-1) for x in [getattr(voxetObject, 'stepvx'), getattr(voxetObject, 'stepvy'), getattr(voxetObject, 'stepvz')]]

    if nw == 1:
        jsonData['W'] = [getattr(voxetObject, 'stepwx'), getattr(voxetObject, 'stepwy'), getattr(voxetObject, 'stepwz')]
    else:
        jsonData['W'] = [x/(nw-1) for x in [getattr(voxetObject, 'stepwx'), getattr(voxetObject, 'stepwy'), getattr(voxetObject, 'stepwz')]]
    jsonData['DIR_NUM'] = [nu, nv, nw]

    result = {'Voxetdata': jsonData, 'PROPERTY':ValuesQuerySetToDict(propertyData), 'REGION': ValuesQuerySetToDict(regionData)}
    return result

def index(request):
    return render_to_response('index.html', context_instance=RequestContext(request))

def getdatalist(request):
    modelList = []
    baseList = Odbbase.objects.exclude(typeid = 'OdbProject').exclude(typeid = 'OdbVoxetProperty').exclude(typeid =  'OdbVoxetRegion')
    for base in baseList:
        obj = get_child_object(base)
        className = obj.__class__.__name__
        objType = getattr(obj, 'typeid')
        objID = getattr(obj, 'gobjid2')
        if (objType == 'OdbModel3d'):
            objName = getattr(obj, 'objectname')
        elif (objType == 'OdbProject'):
            objName = getattr(obj, 'projectname')
        elif (objType == 'OdbVoxet'):
            objName = getattr(obj, 'voxetname')
        elif (objType == 'OdbSurvey'):
            objName = getattr(obj, 'surveyname')
        elif (objType == 'OdbWell'):
            objName = getattr(obj, 'holeno')
        elif (objType == 'OdbGObj'):
            objName = getattr(obj, 'objectname')
        else:
            objName = 'null'

        objData = {'id':objID, 'name':objName, 'type':objType}
        modelList.append(objData)

    response_dict = {'total':len(modelList),'rows':modelList}
    #aaa = json.dumps(response_dict)
    return HttpResponse(json.dumps(response_dict), content_type="application/json")

def getObjData(request):
    if request.POST.has_key('ObjID'):
        id = request.POST['ObjID']
        base = Odbbase(gobjid2=id)
        obj = get_child_object(base)
        className = obj.__class__.__name__

        if(className == 'Model3D'):
            jsondata = {}
            jsondata = getattr(obj, 'json')
            jsonDic = json.loads(jsondata)
            return HttpResponse(json.dumps(jsonDic), content_type="application/json")
        elif(className == 'Voxet'):
            propertyObject = obj.odbvoxetproperty_set.values('gobjid', 'propertyname', 'propertykind', 'datatype')
            regionObject = obj.odbvoxetregion_set.values('gobjid', 'regionname')

            if(propertyObject[0]['datatype'] == "RGBA" or propertyObject[0]['datatype'] == "Octet"):
                imageObject = obj.odbvoxetproperty_set.values('blob')
                jsonDic = MakeVoxetHeader(obj, propertyObject[0]['datatype'], imageObject[0]['blob'])
            else:
                jsonDic = MakeVoxetJson(obj, propertyObject, regionObject)
            return HttpResponse(json.dumps(jsonDic), content_type="application/json")
        elif(className == 'Well'):
            jsonDic = {field.name: getattr(obj,field.name) for field in obj._meta.fields if not (field.name == 'gobjid' or field.name == 'projectid')}
            return HttpResponse(json.dumps(jsonDic), content_type="application/json")
        elif(className == "Gobj"):
            jsondata = getattr(obj, 'json')
            startIndex = jsondata.find('VRTX')
            endIndex = jsondata.rfind(']')
            part1 = jsondata[0:startIndex]
            part2 = jsondata[startIndex:endIndex]
            part2 = part2.replace("{", "[")
            part2 = part2.replace("}", "]")
            part3 = jsondata[endIndex:]
            jsondata = part1 + part2 + part3

            jsonDic = json.loads(jsondata)
            return HttpResponse(json.dumps(jsonDic), content_type="application/json")
        elif(className == 'Survey'):
            sgridName = getattr(obj, 'surveyname')
            startX = getattr(obj, 'startx')
            startY = getattr(obj, 'starty')
            startZ = getattr(obj, 'startz')
            endX = getattr(obj, 'endx')
            endY = getattr(obj, 'endy')
            endZ = getattr(obj, 'endz')
            blobData = getattr(obj, 'blob')

            reader = SNTRRead(blobData)
            reader.setStartXY(startX, startY)
            reader.setEndXY(endX, endY)

            #####
            x_nodes_size = reader.m_NxNode
            z_nodes_size = reader.m_NzNode
            v_axis_scale = 0.01

            sgrid_CellSize = [x_nodes_size, 2, z_nodes_size]

            surveyline = SurveyLine(startX, startY, endX, endY, 0)

            nu_cell = sgrid_CellSize[0]
            nw_cell = sgrid_CellSize[2]

            unitVec = surveyline.V_unitVec()

            pt1 = reader.getPoint3d(0, 0)
            pt2 = reader.getPoint3d(0, x_nodes_size-1)

            surveline_length = surveyline.LineLength(pt1, pt2)

            sgridNode = []
            for z in range(0, reader.m_NzNode):
                v1 = []
                v2 = []
                for x in range(0, reader.m_NxNode):
                    gpt = reader.getPoint3d(z, x)
                    gpt2 = gpt + unitVec*surveline_length*v_axis_scale
                    v1.append(gpt)
                    v2.append([gpt2[0], gpt2[1], gpt2[2]])
                sgridNode.extend(v1)
                sgridNode.extend(v2)

            sgridProperty = []
            for z in range(0, reader.m_NzNode-1):
                for x in range(0, reader.m_NxNode-1):
                    sgridProperty.append(reader.getCellValue(z, x))
            ####
            JsonData = {'SGriddata': {"Name":sgridName, "DIR_NUM": sgrid_CellSize}, 'Node': sgridNode, 'PROPERTY': sgridProperty}
            return HttpResponse(json.dumps(JsonData), content_type="application/json")

def classifySortKey(item):
    return item[0];

def getVoxetProperty(request):
    if request.POST.has_key('ObjID'):
        id = request.POST['ObjID']
        base = Odbbase(gobjid2=id)
        obj = get_child_object(base)
        propertyData = obj.blob
        propertyType = obj.datatype

        classifi = obj.classification
        classfy = []
        if classifi != None:
            classifyList = classifi.split(';')
            x = iter(classifyList)
            numClassify = int(x.next());

            for i in range(numClassify):
                scalarValue = int(x.next());
                scalarName = x.next();
                scalarColor = x.next();
                x.next();
                classfy.append((scalarValue, scalarName, scalarColor))

        classfy = sorted(classfy, key=classifySortKey)

        cellSize = struct.unpack_from('>l', propertyData[0:4])

        if propertyType == 'Float':
            cellLength = cellSize[0]/4
            unpackFormat = '<%df' % (cellLength)
            cellData = struct.unpack_from(unpackFormat, propertyData[4:])
        elif propertyType == 'Short':
            cellLength = cellSize[0]/2
            unpackFormat = '<%dH' % (cellLength)
            cellData = struct.unpack_from(unpackFormat, propertyData[4:])

        JsonData = {'CellSize': cellLength, 'CellData': cellData, 'Classification':classfy}

        return HttpResponse(json.dumps(JsonData), content_type="application/json")