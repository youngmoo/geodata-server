__author__ = 'fourm'
import struct
import numpy as np

class SNTRRead:
    surverVersion = 0
    inversionParFEM = []
    m_Method = 0
    m_RMSError = []
    m_N2D = []
    m_D = []
    m_R2D = []
    m_NoOfRho = 0
    m_Resis = []
    m_NoOfWaterBlock = 0
    m_ResisWater = []
    m_NxNode = 0
    m_NzNode = 0
    m_KUnit = 0
    m_XNode = []
    m_ZNode = []
    m_ICode = []
    m_Startpoint = []
    m_Endpoint = []
    m_DpSpace = 0

    def __init__(self, blobData):
        unpackData = struct.unpack_from('<i', blobData[0:4])
        if unpackData[0] != 5000000:
            return
        self.surverVersion = unpackData[0]

        self.inversionParFEM = struct.unpack_from('<iififfiiiffifffi', blobData[4:(2048+4)])

        dataIndex = 2052
        self.m_Method = struct.unpack_from('<i', blobData[dataIndex:(dataIndex+4)])[0]
        dataIndex = dataIndex+4

        for i in range(0, self.inversionParFEM[1]+1):
            self.m_N2D = struct.unpack_from('<ii', blobData[dataIndex:(dataIndex+8)])
            dataIndex = dataIndex+8

            self.m_D = struct.unpack_from('<ff', blobData[dataIndex:(dataIndex+8)])
            dataIndex = dataIndex+8

            unpackData = struct.unpack_from('<f', blobData[dataIndex:(dataIndex+4)])
            self.m_RMSError.extend(list(unpackData))
            dataIndex = dataIndex+4

            cellSize = self.m_N2D[0]*self.m_N2D[1]
            self.m_R2D = struct.unpack_from('<%df'%cellSize, blobData[dataIndex:(dataIndex + 4*cellSize)])
            dataIndex = dataIndex + 4*cellSize

            self.m_NoOfRho = struct.unpack_from('<i', blobData[dataIndex:(dataIndex+4)])[0]
            dataIndex = dataIndex+4

            self.m_Resis = struct.unpack_from('<%df'%self.m_NoOfRho, blobData[dataIndex:(dataIndex+4*self.m_NoOfRho)])
            dataIndex = dataIndex+4*self.m_NoOfRho

            self.m_NoOfWaterBlock = struct.unpack_from('<i', blobData[dataIndex:(dataIndex+4)])[0]
            dataIndex = dataIndex+4

            if self.m_NoOfWaterBlock > 0:
                self.m_ResisWater = struct.unpack_from('<%df'%self.m_NoOfWaterBlock, blobData[dataIndex:(dataIndex+4*self.m_NoOfWaterBlock)])
                dataIndex = dataIndex+4*self.m_NoOfWaterBlock

        self.m_NxNode = struct.unpack_from('<i', blobData[dataIndex:(dataIndex+4)])[0]
        dataIndex = dataIndex+4

        self.m_NzNode = struct.unpack_from('<i', blobData[dataIndex:(dataIndex+4)])[0]
        dataIndex = dataIndex+4

        self.m_KUnit = struct.unpack_from('<i', blobData[dataIndex:(dataIndex+4)])[0]
        dataIndex = dataIndex+4

        nodeSize = self.m_NxNode*self.m_NzNode
        self.m_XNode = struct.unpack_from('<%df'%nodeSize, blobData[dataIndex:(dataIndex+4*nodeSize)])
        dataIndex = dataIndex+4*nodeSize

        self.m_ZNode = struct.unpack_from('<%df'%nodeSize, blobData[dataIndex:(dataIndex+4*nodeSize)])
        dataIndex = dataIndex+4*nodeSize

        nodeSize = (self.m_NxNode-1)*(self.m_NzNode-1)
        self.m_ICode = struct.unpack_from('<%di'%nodeSize, blobData[dataIndex:(dataIndex+4*nodeSize)])
        dataIndex = dataIndex+4*nodeSize

        m_IsThereWater = struct.unpack_from('<i', blobData[dataIndex:(dataIndex+4)])[0]
        dataIndex = dataIndex+4

        nodeSize = self.m_NxNode
        m_WaterDepth = struct.unpack_from('<%df'%nodeSize, blobData[dataIndex:(dataIndex+4*nodeSize)])
        dataIndex = dataIndex+4*nodeSize + 20 #20 dummy
        self.m_DpSpace = struct.unpack_from('<f', blobData[dataIndex:(dataIndex+4)])[0]
        dataIndex = dataIndex+4


    def setStartXY(self, x, y):
        self.m_Startpoint = [x, y]
    def setEndXY(self, x, y):
        self.m_Endpoint = [x, y]

    def getPoint3d(self, row, col):
        dirVec = np.subtract(self.m_Endpoint, self.m_Startpoint);
        mag = np.linalg.norm(dirVec)

        unitVec = dirVec/mag;

        positionVec = unitVec*self.m_XNode[row*self.m_NxNode +col]*self.m_DpSpace;
        x = self.m_Startpoint[0] + positionVec[0];
        y = self.m_Startpoint[1] + positionVec[1];
        z = self.m_ZNode[row*self.m_NxNode +col]*self.m_DpSpace;

        return [x, y, z]

    def getCellValue(self, row, col):
        return self.m_R2D[row*self.m_NxNode +col]


