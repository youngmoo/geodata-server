from django.db import models

import base64


# Create your models here.
class Odbbase(models.Model):
    comments = models.TextField(blank=True, null=True)
    user = models.TextField(blank=True, null=True)
    version = models.IntegerField()
    datemodified = models.TextField(db_column='dateModified', blank=True, null=True)  # Field name made lowercase.
    gobjid2 = models.AutoField(db_column='gobjId', primary_key=True)  # Field name made lowercase.
    typeid = models.TextField()

    class Meta:
        managed = False
        db_table = 'OdbBase'

class Project(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)
    #gobjid = models.IntegerField(db_column='gobjId', primary_key=True)  # Field name made lowercase.
    projectname = models.TextField(db_column='projectName', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'project'

class Model3D(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)  # Field name made lowercase.
    objectname = models.TextField(db_column='objectName', blank=True, null=True)  # Field name made lowercase.
    objectdata = models.TextField(db_column='objectData', blank=True, null=True)  # Field name made lowercase.
    json = models.TextField(db_column='json', blank=True, null=True)
    projectid = models.ForeignKey(Project, db_column='project')

    def __unicode__(self):
        return self.json, self.objectname, self.objectdata

    class Meta:
        managed = False
        db_table = 'model3d'

class Gobj(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)  # Field name made lowercase.
    objecttype = models.TextField(db_column='objectType', blank=True, null=True)  # Field name made lowercase.
    objectname = models.TextField(db_column='objectName', blank=True, null=True)  # Field name made lowercase.
    feature = models.TextField(blank=True, null=True)
    objectdata = models.TextField(db_column='objectData', blank=True, null=True)  # Field name made lowercase.
    projectid = models.ForeignKey(Project, db_column='project')
    json = models.TextField(db_column='json', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gobj'

class Survey(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)  # Field name made lowercase.
    surveytype = models.TextField(db_column='surveyType', blank=True, null=True)  # Field name made lowercase.
    surveyname = models.TextField(db_column='surveyName', blank=True, null=True)  # Field name made lowercase.
    blob = models.BinaryField(blank=True, null=True)
    startx = models.FloatField(blank=True, null=True)
    starty = models.FloatField(blank=True, null=True)
    startz = models.FloatField(blank=True, null=True)
    endx = models.FloatField(blank=True, null=True)
    endy = models.FloatField(blank=True, null=True)
    endz = models.FloatField(blank=True, null=True)
    projectid = models.ForeignKey(Project, db_column='project')

    class Meta:
        managed = False
        db_table = 'survey'

class Voxet(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)  # Field name made lowercase.
    nu = models.IntegerField()
    nv = models.IntegerField()
    nw = models.IntegerField()
    originx = models.FloatField(blank=True, null=True)
    originy = models.FloatField(blank=True, null=True)
    originz = models.FloatField(blank=True, null=True)
    stepux = models.FloatField(blank=True, null=True)
    stepuy = models.FloatField(blank=True, null=True)
    stepuz = models.FloatField(blank=True, null=True)
    stepvx = models.FloatField(blank=True, null=True)
    stepvy = models.FloatField(blank=True, null=True)
    stepvz = models.FloatField(blank=True, null=True)
    stepwx = models.FloatField(blank=True, null=True)
    stepwy = models.FloatField(blank=True, null=True)
    stepwz = models.FloatField(blank=True, null=True)
    voxetname = models.TextField(blank=True, null=True)
    modelingrole = models.TextField(blank=True, null=True)
    projectid = models.ForeignKey(Project, db_column='project')

    class Meta:
        managed = False
        db_table = 'voxet'

class Odbvoxetproperty(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)  # Field name made lowercase.
    propertyname = models.TextField(blank=True, null=True)
    propertykind = models.TextField(blank=True, null=True)
    family = models.TextField(blank=True, null=True)
    propertyclass = models.TextField(blank=True, null=True)
    propertyclasstype = models.TextField(db_column='propertyClassType', blank=True, null=True)  # Field name made lowercase.
    blob = models.BinaryField(blank=True, null=True)
    interpolation = models.TextField(blank=True, null=True)
    elementcount = models.IntegerField()
    classification = models.TextField(blank=True, null=True)
    datatype = models.TextField(blank=True, null=True)
    ndv = models.FloatField(blank=True, null=True)
    voxetid = models.ForeignKey(Voxet, db_column='voxet')

    class Meta:
        managed = False
        db_table = 'OdbVoxetProperty'

class Odbvoxetregion(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)  # Field name made lowercase.
    regionname = models.TextField(blank=True, null=True)
    blob = models.BinaryField(blank=True, null=True)
    voxetid = models.ForeignKey(Voxet, db_column='voxet')

    class Meta:
        managed = False
        db_table = 'OdbVoxetRegion'

class Well(Odbbase):
    gobjid = models.OneToOneField(Odbbase, db_column='gobjId', primary_key=True, parent_link=True)  # Field name made lowercase.
    holeno = models.TextField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    x = models.FloatField(blank=True, null=True)
    y = models.FloatField(blank=True, null=True)
    z = models.FloatField(blank=True, null=True)
    dir = models.IntegerField()
    angle = models.IntegerField()
    projectid = models.ForeignKey(Project, db_column='project')

    class Meta:
        managed = False
        db_table = 'well'

class Wellset(models.Model):
    wellsetid = models.AutoField(primary_key=True)
    setname = models.TextField(blank=True, null=True)
    interpolation = models.TextField(blank=True, null=True)
    wellid = models.ForeignKey(Well, db_column='well')

    class Meta:
        managed = False
        db_table = 'WellSet'

class Welllog(models.Model):
    welllogid = models.AutoField(primary_key=True)
    logname = models.TextField(db_column='logName')  # Field name made lowercase.
    datatype = models.TextField()
    user = models.TextField()
    wellsetid = models.ForeignKey(Wellset, db_column='wellset')

    class Meta:
        managed = False
        db_table = 'WellLog'

class Welllogvalue(models.Model):
    welllogvalueid = models.AutoField(db_column='wellLogValueId', primary_key=True)  # Field name made lowercase.
    typeid = models.TextField()
    welllogid = models.ForeignKey(Welllog, db_column='wellLog')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'WellLogValue'

class Logvaluedouble(Welllogvalue):
    welllogvalueid2 = models.OneToOneField(Welllogvalue, db_column='wellLogValueId', primary_key=True, parent_link=True)
    depth = models.FloatField()
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LogValueDouble'

class Logvaluetext(Welllogvalue):
    welllogvalueid2 = models.OneToOneField(Welllogvalue, db_column='wellLogValueId', primary_key=True, parent_link=True)
    depth = models.FloatField()
    text = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LogValueText'